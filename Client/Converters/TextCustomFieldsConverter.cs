﻿using System;
using System.Linq;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Windows.UI.Xaml.Data;

namespace Quest.Cyclone.Client.Windows
{
    public class TextCustomFieldConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null)
            {
                var fields = (CustomField[])value;
                return fields.Where(field => field.Type == CustomFieldType.Text).ToList();
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
