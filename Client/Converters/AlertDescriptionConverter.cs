﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Windows.UI.Xaml.Data;

namespace Quest.Cyclone.Client.Windows
{
    public class AlertDescriptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null)
            {
                var description = (string)value;
                var result = description;
                var match = new Regex("(%((\\w|\\W)*?)%)").Match(description);
                while (match.Success)
                {
                    result = result.Replace(match.Groups[1].Value, match.Groups[2].Value);
                    match = match.NextMatch();
                }

                return result;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

}
