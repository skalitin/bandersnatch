﻿using System;
using Quest.Cyclone.Client.Windows.Commons;
using Windows.UI.Xaml.Data;

namespace Quest.Cyclone.Client.Windows
{
    public class TimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
            {
                return null;
            }

            var time = (long)value;

            var dateTime = Utils.GetCurrentTimeFromUtcTicks(time);
            if (DateTime.Now.StartOfWeek(DayOfWeek.Monday).CompareTo(dateTime) > 0)
            {
                return String.Format("{0:MM/dd/yy}", dateTime);
            }

            return String.Format(DateTime.Today.CompareTo(dateTime) > 0 ? "{0:ddd}" : "{0:HH:mm}", dateTime);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
