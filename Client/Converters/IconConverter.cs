﻿using System;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;

namespace Quest.Cyclone.Client.Windows
{
    public class IconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null)
            {
                var icon = (Icon)value;

                if (icon.Data != null)
                {
                    using (var stream = new InMemoryRandomAccessStream())
                    {
                        var writer = new DataWriter(stream.GetOutputStreamAt(0));
                        writer.WriteBytes(icon.Data);
                        writer.StoreAsync().AsTask().Wait();
                        writer.FlushAsync().AsTask().Wait();

                        var image = new BitmapImage();
                        image.SetSource(stream);

                        return image;
                    }
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
