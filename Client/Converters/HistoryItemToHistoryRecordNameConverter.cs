﻿using System;
using System.Linq;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Windows.UI.Xaml.Data;

namespace Quest.Cyclone.Client.Windows
{
    public class HistoryItemToHistoryRecordNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
            {
                return null;
            }

            var item = (HistoryItem)value;
            if (Execute.InDesignMode)
            {
                return item.Type;
            }

            string data;
            switch (item.Type)
            {
                case HistoryItemType.Created:
                    data = Utils.GetStringFromResource("HistoryItemNameCreated");
                    break;
                case HistoryItemType.StatusChanged:
                    data = Utils.GetStringFromResource("HistoryItemNameStatusChanged");
                    break;
                case HistoryItemType.OwnerChanged:
                    data = Utils.GetStringFromResource("HistoryItemNameOwnerChanged");
                    break;
                case HistoryItemType.ActionStarted:
                    data = Utils.GetStringFromResource("HistoryItemNameActionStarted");
                    break;
                case HistoryItemType.ActionFinished:
                    if (Boolean.Parse(item.Properties.First(prop => prop.Name == HistoryItemProperty.Success).Value))
                    {
                        data = Utils.GetStringFromResource("HistoryItemNameActionFinished");
                    }
                    else
                    {
                        data = Utils.GetStringFromResource("HistoryItemNameActionFailed");
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return data;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
