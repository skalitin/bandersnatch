﻿using System;
using Quest.Cyclone.Client.Windows.Commons;
using Windows.UI.Xaml.Data;

namespace Quest.Cyclone.Client.Windows
{
    public class BadgeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
            {
                return null;
            }

            var number = (int)value;
            if (number == 0)
            {
                return null;
            }

            if (number > 999)
            {
                return "999";
            }

            return number.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
