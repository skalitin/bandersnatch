﻿
using System;
using Caliburn.Micro;
using Windows.UI.Xaml.Data;

namespace Quest.Cyclone.Client.Windows
{
    public class BooleanToVisibilityReverseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var converter = new BooleanToVisibilityConverter();
            return converter.Convert(!(bool)value, targetType, parameter, language);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
