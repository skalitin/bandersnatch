﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Quest.Cyclone.Client.Windows
{
    public class StringToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return String.IsNullOrEmpty(value as String) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

}
