﻿using System;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Windows.UI.Xaml.Data;

namespace Quest.Cyclone.Client.Windows
{
    public class NewAlertStatusToActiveTabConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return ((AlertStatus)value) == AlertStatus.New;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class AckAlertStatusToActiveTabConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return ((AlertStatus)value) == AlertStatus.Acknowledged;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class ResolvedAlertStatusToActiveTabConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return ((AlertStatus)value) == AlertStatus.Resolved;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
