﻿using System;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Quest.Cyclone.Client.Windows
{
    public class AlertStateToAcknowledgeButtonVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var status = (AlertStatus) value;
            return status == AlertStatus.New ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class AlertStateToResolveButtonVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var status = (AlertStatus)value;
            return status == AlertStatus.New || status == AlertStatus.Acknowledged ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class AlertStateToActionsButtonVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var status = (AlertStatus)value;
            return status == AlertStatus.New || status == AlertStatus.Acknowledged ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class AlertFullDescriptionToActionsButtonVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var description = (AlertFullDescription)value;
            return  description == null ||
                    description.Actions == null ||
                    description.Actions.IsEmpty() 
                        ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
