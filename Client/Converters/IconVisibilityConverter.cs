﻿using System;
using System.Globalization;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;

namespace Quest.Cyclone.Client.Windows
{
    public class IconVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is Icon)
            {
                var icon = (Icon)value;
                return !String.IsNullOrEmpty(icon.Id) ? Visibility.Visible : Visibility.Collapsed;
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
