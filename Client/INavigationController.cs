﻿namespace Quest.Cyclone.Client.Windows
{
    public interface INavigationController
    {
        void SignOut();
        void RestartGettingUpdates();
    }
}