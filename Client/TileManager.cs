﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using NotificationsExtensions.BadgeContent;
using NotificationsExtensions.TileContent;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Storages;
using Windows.Foundation;
using Windows.UI.Notifications;
using Windows.UI.StartScreen;

namespace Quest.Cyclone.Client.Windows
{
    public class TileManager : IHandle<NewAlertsChangedMessage>, IHandle<PackageMessage>, IHandle<SettingMessage>, ITileManager
    {
        private readonly IAlertStorage _alertStorage;
        private readonly TileUpdater _tileUpdater;
        private readonly ISettings _settings;
        private readonly BadgeUpdater _badgeUpdater;
        private readonly IPackageStorage _packageStorage;

        public TileManager(IAlertStorage alertStorage, IPackageStorage packageStorage, ISettings settings)
        {
            _alertStorage = alertStorage;
            _settings = settings;
            _packageStorage = packageStorage;
            
            _badgeUpdater = BadgeUpdateManager.CreateBadgeUpdaterForApplication();
            _tileUpdater = TileUpdateManager.CreateTileUpdaterForApplication();
            _tileUpdater.EnableNotificationQueue(true);
        }

        public async Task<bool> PinPackageAsync(Package package)
        {
            var secondaryTile = CreateSecondaryTile(package);
            return await secondaryTile.RequestCreateAsync();
        }

        public async Task<bool> UnpinPackageAsync(Package package)
        {
            var secondaryTile = CreateSecondaryTile(package);
            return await secondaryTile.RequestDeleteAsync();
        }

        private SecondaryTile CreateSecondaryTile(Package package)
        {
            var tileId = GetSecondaryTileId(package);
            var shortName = package.Title;
            var displayName = String.Format("Mobile IT\n{0}", package.Title);
            var arguments = package.Id.ToString();
            return new SecondaryTile(tileId, shortName, displayName, arguments,
                TileOptions.ShowNameOnLogo, new Uri("ms-appx:///assets/logo.png"));
        }

        public bool IsPackagePinned(Package package)
        {
            return SecondaryTile.Exists(GetSecondaryTileId(package));
        }

        public Guid GetPinnedPackageId(string tileId, string arguments)
        {
            if (tileId.StartsWith("mobileit-package-"))
            {
                return Guid.Parse(arguments);
            }

            return Guid.Empty;
        }

        private string GetSecondaryTileId(Package package)
        {
            return String.Format("{0}{1}", "mobileit-package-", package.Id);
        }

        private Guid GetPackageIdFromSecondaryTileId(string tileId)
        {
            return Guid.Parse(tileId.Substring("mobileit-package-".Length, Guid.Empty.ToString().Length));
        }

        public void Handle(SettingMessage settingMessage)
        {
            if (settingMessage.Is(() => _settings.IsLoggedOn) && !_settings.IsLoggedOn)
            {
                _tileUpdater.Clear();
                SetBadge(0);
            }
        }

        public void Handle(NewAlertsChangedMessage message)
        {
            _tileUpdater.Clear();

            var alerts = _alertStorage.GetAlerts(AlertStatus.New);
            var random = new Random();
            for (var i = 0; i < Math.Min(5, alerts.Count); i++)
            {
                var alert = alerts.ElementAt(random.Next(alerts.Count()));
                SetLiveTile(alert.Brief.Title, alert.Brief.Description);
            }
        }

        private void SetBadge(int badge)
        {
            var badgeNotification = new BadgeNumericNotificationContent((uint) badge).CreateNotification();
            _badgeUpdater.Update(badgeNotification);
        }

        private void SetLiveTile(string header, string text)
        {
            var tileContent = TileContentFactory.CreateTileWideText09();
            tileContent.TextHeading.Text = header;
            tileContent.TextBodyWrap.Text = text;

            var squareTileContent = TileContentFactory.CreateTileSquareText04();
            squareTileContent.TextBodyWrap.Text = header;
            tileContent.SquareContent = squareTileContent;

            var tileNotification = tileContent.CreateNotification();
            _tileUpdater.Update(tileNotification);            
        }

        public void Handle(PackageMessage packageMessage)
        {
            if (packageMessage is PackageCountChangedMessage || packageMessage is PackageBadgeChangedMessage)
            {
                var badge = _packageStorage.Packages.Sum(each => each.Badge);
                SetBadge(badge);
            }
        }
    }
}
