﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Samples;
using Quest.Cyclone.Client.Windows.Commons.Tasks;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class AlertActionViewModel : ViewModelBase, IHandle<TaskMessage>
    {
        private readonly IAlertStorage _alertStorage;
        private readonly ISettings _settings;
        private readonly IServerRequestsManager _serverRequestsManager;
        private readonly ITaskManager _taskManager;
        private ObservableCollection<HistoryItem> _historyItems;

        public AlertActionViewModel()
        {
            if (Execute.InDesignMode)
            {
                Alert = Samples.Alert;
                Action = Alert.Full.Actions.ToArray().First();
            }
        }

        public AlertActionViewModel(IAlertStorage alertStorage, ISettings settings, IServerRequestsManager serverRequestsManager, ITaskManager taskManager, INavigationService navigationService)
            : base(navigationService)
        {
            _alertStorage = alertStorage;
            _settings = settings;
            _serverRequestsManager = serverRequestsManager;
            _taskManager = taskManager;
        }

        public Guid PackageId { get; set; }
        public Alert Alert { get; set; }
        public ActionDefinition Action { get; set; }

        public ObservableCollection<HistoryItem> HistoryItems
        {
            get
            {
                return _historyItems ?? (_historyItems = new ObservableCollection<HistoryItem>(GetCurrentHistoryItems()));
            }
            set
            {
                _historyItems = value;
                NotifyOfPropertyChange(() => HistoryItems);
            }
        }

        private IEnumerable<HistoryItem> GetCurrentHistoryItems()
        {
            return Alert.HistoryItems.Where(item =>
                                            item.Properties.Any(property =>
                                                                property.Name == HistoryItemProperty.ActionId &&
                                                                Guid.Parse(property.Value) == Action.Id)).ToList();
        }

        public bool ActionUnavailable
        {
            get
            {
                return 
                    _taskManager.IsTaskRunning<RunAlertActionTask>(task => task.Uses(Alert)) ||
                    Alert.Brief.Status == AlertStatus.Resolved ||
                    Alert.Brief.State == RunState.InProgress;
            }
        }

        public void Run()
        {
            var task = new RunAlertActionTask(
                _settings, 
                _serverRequestsManager, 
                _alertStorage, 
                PackageId, 
                Alert.Id,                    
                Action.Id);

            _taskManager.RunTask(task);
        }

        public void Handle(TaskMessage message)
        {
            if (message.Task is RunAlertActionTask ||
                message.Task is GetAlertDetailsTask ||
                message.Task is GetUpdatesTask
                )
            {
                NotifyOfPropertyChange(() => ActionUnavailable);
                UpdateHistoryItems();
            }
        }

        private void UpdateHistoryItems()
        {
            var newHistoryItems = GetCurrentHistoryItems()
                .Where(currentItem => HistoryItems.All(item => item.Time != currentItem.Time))
                .ToList();

            newHistoryItems.Reverse();
            foreach (var item in newHistoryItems)
            {
                HistoryItems.Insert(0, item);
            }
        }
    }
}
