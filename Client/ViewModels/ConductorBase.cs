﻿using System;
using Caliburn.Micro;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public abstract class ConductorBase<T> : Conductor<T>.Collection.OneActive where T: class 
    {
        protected readonly INavigationService _navigationService;

        protected ConductorBase()
        {
            if (!Execute.InDesignMode)
            {
                throw new InvalidOperationException("Parameterless constructor intended to design mode only.");
            }
        }

        protected ConductorBase(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public void GoBack()
        {
            _navigationService.GoBack();
        }

        public bool CanGoBack
        {
            get { return _navigationService.CanGoBack; }
        }
    }
}
