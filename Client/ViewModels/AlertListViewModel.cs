﻿using System;
using System.Linq;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Samples;
using Quest.Cyclone.Client.Windows.Commons.Storages;
using Quest.Cyclone.Client.Windows.Commons.Tasks;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class AlertListViewModel : ViewModelBase, IHandle<TaskMessage>, IRefresh
    {
        private readonly Guid _packageId;
        private readonly IAlertStorage _alertStorage;
        private readonly IUIManager _iuiManager;
        private readonly ITaskManager _taskManager;
        private readonly IServerRequestsManager _serverRequestsManager;
        private readonly ISettings _settings;
        private readonly IEventAggregator _eventAggregator;
        private readonly AlertStatus _status;
        private Alert _selectedAlert;
        private AlertDetailsViewModel _alertDetailsViewModel;

        public AlertListViewModel()
        {
            if (Execute.InDesignMode)
            {
                _status = AlertStatus.New;
                _eventAggregator = new EventAggregator();
                _alertStorage = new AlertStorage(null, _eventAggregator);
                _alertStorage.SetAlerts(Guid.Empty, new[]{ Samples.Alert, Samples.Alert, Samples.Alert });
            }
        }

        public AlertListViewModel(Guid packageId, AlertStatus status, ITaskManager taskManager, IAlertStorage alertStorage, IUIManager iuiManager, INavigationService navigationService, ISettings settings, IServerRequestsManager serverRequestsManager, IEventAggregator eventAggregator)
            : base(navigationService)
        {
            _serverRequestsManager = serverRequestsManager;
            _settings = settings;
            _packageId = packageId;
            _alertStorage = alertStorage;
            _iuiManager = iuiManager;
            _status = status;
            _taskManager = taskManager;
            _eventAggregator = eventAggregator;

            _eventAggregator.Subscribe(this);
            
            AlertDetailsViewModel = CreateAlertDetailsViewModel(SelectedAlert);
        }

        public bool LoadingInProgress
        {
            get
            {
                switch (_status)
                {
                    case AlertStatus.New:
                        return _taskManager.IsTaskRunning<GetNewAlertsTask>();
                    case AlertStatus.Acknowledged:
                        return _taskManager.IsTaskRunning<GetAckAlertsTask>();
                    case AlertStatus.Resolved:
                        return _taskManager.IsTaskRunning<GetResolvedAlertsTask>();
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public AlertStatus Status
        {
            get { return _status; }
        }

        public BindableCollection<Alert> Alerts 
        {
            get { return _alertStorage.GetAlerts(_packageId, Status); }
        }

        public Alert SelectedAlert
        {
            get
            {
                return _selectedAlert;
            }
            set
            {
                _selectedAlert = value;
                NotifyOfPropertyChange(() => SelectedAlert);

                // Don't recreate alert details view when alert list lost selection
                if (_selectedAlert != null)
                {
                    AlertDetailsViewModel = CreateAlertDetailsViewModel(_selectedAlert);
                }
            }
        }

        private AlertDetailsViewModel CreateAlertDetailsViewModel(Alert alert)
        {
            return new AlertDetailsViewModel(
                _navigationService,
                _iuiManager,
                _settings,
                _serverRequestsManager,
                _alertStorage,
                _taskManager,
                _eventAggregator,
                alert,
                _packageId);
        }

        public AlertDetailsViewModel AlertDetailsViewModel
        {
            get
            {
                return _alertDetailsViewModel;
            }
            set
            {
                _alertDetailsViewModel = value;
                NotifyOfPropertyChange(() => AlertDetailsViewModel);
            }
        }

        public void Handle(TaskMessage message)
        {
            if (message.Task is GetAlertsTask)
            {
                NotifyOfPropertyChange(() => LoadingInProgress);

                if (message is TaskFinishedMessage && (message.Task as GetAlertsTask).Uses(_status))
                {
                    SelectedAlert = AlertDetailsViewModel.Alert ?? Alerts.FirstOrDefault();
                }
            }
        }

        // Called when user selects corresponding tab in UI
        public new void OnActivate()
        {
            if (SelectedAlert == null || SelectedAlert.Brief.Status != _status)
            {
                SelectedAlert = Alerts.FirstOrDefault();
                if (SelectedAlert == null)
                {
                    AlertDetailsViewModel = CreateAlertDetailsViewModel(null);
                }
            }
        }

        public void RefreshData(bool initial)
        {
            if (initial && _alertStorage.HasAlerts(_packageId, _status))
            {
                SelectedAlert = Alerts.FirstOrDefault();
                return;
            }

            TaskBase task = null;
            switch (_status)
            {
                case AlertStatus.New:
                    task = new GetNewAlertsTask(_alertStorage, _settings, _serverRequestsManager, _packageId);
                    break;

                case AlertStatus.Acknowledged:
                    task = new GetAckAlertsTask(_alertStorage, _settings, _serverRequestsManager, _packageId);
                    break;

                case AlertStatus.Resolved:
                    task = new GetResolvedAlertsTask(_alertStorage, _settings, _serverRequestsManager, _packageId);
                    break;
            }

            _alertStorage.RemoveAlerts(_packageId, _status);
            _taskManager.RunTask(_packageId, task);
        }
    }
}
