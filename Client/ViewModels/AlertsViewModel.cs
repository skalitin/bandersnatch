﻿using System;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class AlertsViewModel : ViewModelBase, IRefresh
    {
        private readonly IAlertStorage _alertStorage;

        private AlertListViewModel _newAlertsViewModel;
        private AlertListViewModel _ackAlertsViewModel;
        private AlertListViewModel _resolvedAlertsViewModel;
        private AlertListViewModel _currentTab;
        private readonly Guid _packageId;

        public AlertsViewModel(Guid packageId, ITaskManager taskManager, IUIManager uiManager, IAlertStorage alertStorage, INavigationService navigationService, IEventAggregator eventAggregator, IServerRequestsManager serverRequestsManager, ISettings settings)
            : base(navigationService)
        {
            _alertStorage = alertStorage;
            _packageId = packageId;

            NewAlertsViewModel = new AlertListViewModel(_packageId, AlertStatus.New, taskManager, _alertStorage, uiManager, navigationService, settings, serverRequestsManager, eventAggregator);
            AckAlertsViewModel = new AlertListViewModel(_packageId, AlertStatus.Acknowledged, taskManager, _alertStorage, uiManager, navigationService, settings, serverRequestsManager, eventAggregator);
            ResolvedAlertsViewModel = new AlertListViewModel(_packageId, AlertStatus.Resolved, taskManager, _alertStorage, uiManager, navigationService, settings, serverRequestsManager, eventAggregator);

            CurrentTab = NewAlertsViewModel;
        }

        protected override void OnInitialize()
        {
            RefreshData(true);
            base.OnInitialize();
        }

        public override string DisplayName
        {
            get { return Commons.Utils.GetStringFromResource("Alerts"); }
        }

        public AlertListViewModel NewAlertsViewModel
        {
            get { return _newAlertsViewModel; }
            set
            {
                _newAlertsViewModel = value;
                NotifyOfPropertyChange(() => NewAlertsViewModel);
            }
        }

        public AlertListViewModel AckAlertsViewModel
        {
            get { return _ackAlertsViewModel; }
            set
            {
                _ackAlertsViewModel = value;
                NotifyOfPropertyChange(() => AckAlertsViewModel);
            }
        }

        public AlertListViewModel ResolvedAlertsViewModel
        {
            get { return _resolvedAlertsViewModel; }
            set
            {
                _resolvedAlertsViewModel = value;
                NotifyOfPropertyChange(() => ResolvedAlertsViewModel);
            }
        }

        public AlertListViewModel CurrentTab
        {
            get { return _currentTab; }
            set
            {
                _currentTab = value;
                NotifyOfPropertyChange(() => CurrentTab);
            }
        }

        public void ShowNew()
        {
            CurrentTab = NewAlertsViewModel;
            CurrentTab.OnActivate();
        }

        public void ShowAck()
        {
            CurrentTab = AckAlertsViewModel;
            CurrentTab.OnActivate();
        }

        public void ShowResolved()
        {
            CurrentTab = ResolvedAlertsViewModel;
            CurrentTab.OnActivate();
        }

        public void RefreshData(bool initial)
        {
            NewAlertsViewModel.RefreshData(initial);
            AckAlertsViewModel.RefreshData(initial);
            ResolvedAlertsViewModel.RefreshData(initial);
        }
    }
}
