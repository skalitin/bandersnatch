﻿using System;
using System.Linq;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Samples;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class PackageViewModel : ConductorBase<Screen>, IRefresh
    {
        private readonly IAlertStorage _alertStorage;
        private readonly IPackageStorage _packageStorage;
        private readonly ITaskManager _taskManager;
        private readonly IUIManager _interfaceManager;
        private readonly ISettings _settings;
        private readonly IServerRequestsManager _serverRequestsManager;
        private readonly IEventAggregator _eventAggregator;

        public PackageViewModel()
        {
            if (!Execute.InDesignMode)
            {
                return;
            }
        }

        public PackageViewModel(IAlertStorage alertStorage, IPackageStorage packageStorage, ITaskManager taskManager, IUIManager interfaceManager, INavigationService navigationService, ISettings settings, IServerRequestsManager serverRequestsManager, IEventAggregator eventAggregator)
            :base(navigationService)
        {
            _alertStorage = alertStorage;
            _packageStorage = packageStorage;
            _taskManager = taskManager;
            _interfaceManager = interfaceManager;
            _settings = settings;
            _serverRequestsManager = serverRequestsManager;
            _eventAggregator = eventAggregator;
        }

        protected override void OnActivate()
        {
            ActiveItem = Items.First();
            base.OnActivate();
        }

        protected override void OnInitialize()
        {
            Items.Add(new AlertsViewModel(PackageId, _taskManager, _interfaceManager, _alertStorage, _navigationService, _eventAggregator, _serverRequestsManager, _settings));
            Items.Add(new DatasheetsViewModel(_navigationService, _packageStorage, _taskManager, _eventAggregator, PackageId));
            Items.Add(new HistoryViewModel(_navigationService));
        }

        public Guid PackageId { get; set; }

        public string Title
        { 
            get { return _packageStorage.GetPackage(PackageId).Title; } 
        }

        public void RefreshData(bool initial)
        {
            var selected = ActiveItem as IRefresh;
            if (selected != null)
            {
                selected.RefreshData(initial);
            }
        }
    }
}
