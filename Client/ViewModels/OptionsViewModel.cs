﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using Callisto.Converters;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Tasks;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class TimeSpanWrapper
    {
        public TimeSpanWrapper()
        {
        }

        public TimeSpanWrapper(TimeSpan value)
        {
            Value = value;
        }

        public string Name
        { 
            get
            {
                var resourceId = String.Format("Interval{0}s", Value.TotalSeconds);
                var resourceString = Utils.GetStringFromResource(resourceId);
                return String.IsNullOrEmpty(resourceString) ? Value.ToString("c") : resourceString;
            }
        }

        public TimeSpan Value { get; set; }

        protected bool Equals(TimeSpanWrapper other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.GetType() == GetType() && Equals((TimeSpanWrapper)other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }

    public class OptionsViewModel : SettingsViewModel
    {
        private readonly ISettings _settings;
        private readonly INavigationController _navigationController;
        private readonly ITaskManager _taskManager;

        public OptionsViewModel(INavigationService navigationService, ISettings settings, INavigationController navigationController, ITaskManager taskManager)
            : base(navigationService)
        {
            _settings = settings;
            _navigationController = navigationController;
            _taskManager = taskManager;
        }

        public string UserName
        {
            get
            {
                return _settings.IsAnonymousLogonMode ? Utils.GetStringFromResource("AnonymousUser") : _settings.UserName;
            }
        }

        public string ServerName
        {
            get
            {
                return _settings.ServerName;
            }
        }

        public void SignOut()
        {
            Close();
            _navigationController.SignOut();
        }

        public List<TimeSpanWrapper> UpdateIntervals
        {
            get
            {
                return new List<TimeSpanWrapper>()
                {
                    new TimeSpanWrapper(TimeSpan.FromSeconds(10)),
                    new TimeSpanWrapper(TimeSpan.FromMinutes(1)),
                    new TimeSpanWrapper(TimeSpan.FromMinutes(10)),
                    new TimeSpanWrapper(TimeSpan.FromMinutes(30)),
                };
            }
        }

        public TimeSpanWrapper SelectedUpdateInterval
        {
            get
            {
                return new TimeSpanWrapper(_settings.UpdateInterval);
            }
            set
            {
                var previousValue = _settings.UpdateInterval;
                _settings.UpdateInterval = value.Value;

                if (previousValue != value.Value)
                {
                    _taskManager.RunPeriodicTask<GetUpdatesTask>(value.Value);
                }
            }
        }
    }
}
