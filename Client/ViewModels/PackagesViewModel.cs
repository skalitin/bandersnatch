﻿using System;
using Caliburn.Micro;
using NotificationsExtensions.BadgeContent;
using NotificationsExtensions.TileContent;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Tasks;
using Windows.UI.Notifications;
using Windows.UI.StartScreen;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class PackagesViewModel : ViewModelBase, IHandle<TaskMessage>, IRefresh
    {
        private readonly ITaskManager _taskManager;
        private readonly IPackageStorage _packageStorage;
        private readonly IAlertStorage _alertStorage;
        private readonly ITileManager _tileManager;
        private Package _selectedPackage;

        public PackagesViewModel(INavigationService navigationService, ITaskManager taskManager, IPackageStorage packageStorage, IAlertStorage alertStorage, ITileManager tileManager)
            : base(navigationService)
        {
            _taskManager = taskManager;
            _packageStorage = packageStorage;
            _alertStorage = alertStorage;
            _tileManager = tileManager;
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            RefreshData(initial: true);
        }

        public void RefreshData(bool initial)
        {
            if (!initial)
            {
                _alertStorage.RemoveAlerts();
            }

            _taskManager.RunTask<GetPackagesTask>();
        }

        public bool RefreshInProgress
        {
            get { return _taskManager.IsTaskRunning<GetPackagesTask>(); }
        }

        public BindableCollection<Package> Packages
        {
            get { return _packageStorage.Packages; }
        }

        public Package SelectedPackage
        { 
            get { return _selectedPackage; }
            set
            {
                _selectedPackage = value;
                NotifyOfPropertyChange(() => SelectedPackage);
                NotifyOfPropertyChange(() => CanPinPackage);
                NotifyOfPropertyChange(() => CanUnpinPackage);
            }
        }

        public async void PinPackage()
        {
            if (SelectedPackage != null)
            {
                await _tileManager.PinPackageAsync(SelectedPackage);
                SelectedPackage = null;
            }
        }

        public async void UnpinPackage()
        {
            if (SelectedPackage != null)
            {
                await _tileManager.UnpinPackageAsync(SelectedPackage);
                SelectedPackage = null;
            }
        }

        public bool CanPinPackage
        {
            get { return SelectedPackage != null && !_tileManager.IsPackagePinned(SelectedPackage); }
        }

        public bool CanUnpinPackage
        {
            get { return SelectedPackage != null && _tileManager.IsPackagePinned(SelectedPackage); }
        }

        public void PackageClicked(ItemClickEventArgs eventArgs)
        {
            var package = (Package)eventArgs.ClickedItem;
            _navigationService.UriFor<PackageViewModel>()
                .WithParam(model => model.PackageId, package.Id)
                .Navigate();
        }

        public void Handle(TaskMessage message)
        {
            if (message.Task is GetPackagesTask)
            {
                NotifyOfPropertyChange(() => RefreshInProgress);
            }
        }
    }
}
