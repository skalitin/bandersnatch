﻿using System;
using Caliburn.Micro;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public abstract class ViewModelBase : Screen
    {
        protected readonly INavigationService _navigationService;

        protected ViewModelBase()
        {
            if (!Execute.InDesignMode)
            {
                throw new InvalidOperationException("Parameterless constructor intended to design mode only.");
            }
        }

        protected ViewModelBase(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public void GoBack()
        {
            _navigationService.GoBack();
        }

        public bool CanGoBack
        {
            get { return _navigationService.CanGoBack; }
        }
    }
}
