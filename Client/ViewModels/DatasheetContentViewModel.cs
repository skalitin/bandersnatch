﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Models;
using Quest.Cyclone.Client.Windows.Commons.Samples;
using Quest.Cyclone.Client.Windows.Commons.Tasks;
using Windows.UI.Xaml.Data;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class DatasheetContentConverter : IValueConverter
    {
        public class DatasheetRecordModel
        {
            public DatasheetRecordModel()
            {
                Title = "";
                Body = "";
            }

            public Icon Icon { get; set; }
            public string Title { get; set; }
            public string Body { get; set; }
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null)
            {
                var content = (DatasheetContentModel)value;
                var records = new List<DatasheetRecordModel>();

                foreach (var record in content.Records)
                {
                    var recordModel = new DatasheetRecordModel();
                    for (var pos = 0; pos < content.Columns.Length; ++pos)
                    {
                        var currentValue = record.Cells[pos];
                        var column = content.Columns[pos];

                        if (column.IsPrimary)
                        {
                            recordModel.Title = currentValue;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(currentValue) || string.IsNullOrWhiteSpace(currentValue))
                            {
                                continue;
                            }

                            if (!string.IsNullOrEmpty(recordModel.Body))
                            {
                                recordModel.Body += ", ";
                            }

                            recordModel.Body += column.Name + ": " + currentValue;
                        }
                    }

                    recordModel.Icon = record.Icon;
                    records.Add(recordModel);
                }

                return records;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class DatasheetContentViewModel : Screen, IDatasheetsFrame
    {
        private readonly ITaskManager _taskManager;
        private readonly IEventAggregator _eventAggregator;
        private readonly Guid _instanceId;

        public DatasheetContentViewModel(ITaskManager taskManager, IEventAggregator eventAggregator, Guid instanceId, DatasheetContentModel datasheetContentModel)
        {
            _taskManager = taskManager;
            _instanceId = instanceId;
            _eventAggregator = eventAggregator;
            
            DatasheetContentModel = datasheetContentModel;
            DatasheetRecords = new BindableCollection<DatasheetRecord>(datasheetContentModel.Records);
        }

        public BindableCollection<DatasheetRecord> DatasheetRecords { get; set; }
        public DatasheetContentModel DatasheetContentModel { get; set; }
    }
}
