﻿using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class HistoryViewModel : ViewModelBase
    {
        public HistoryViewModel(INavigationService navigationService) : base(navigationService)
        {
        }

        public override string DisplayName
        {
            get { return Utils.GetStringFromResource("History"); }
        }
    }
}
