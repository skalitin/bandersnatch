﻿using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Tasks;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class DeviceNotApprovedViewModel : ViewModelBase, IHandle<TaskMessage>
    {
        private readonly ISettings _settings;
        private readonly INavigationController _navigationController;
        private readonly ITaskManager _taskManager;

        public DeviceNotApprovedViewModel(INavigationService navigationService, ISettings settings, INavigationController navigationController, ITaskManager taskManager)
            : base(navigationService)
        {
            _settings = settings;
            _navigationController = navigationController;
            _taskManager = taskManager;
        }

        public string Message
        {
            get { return _settings.DeviceStatus.Message; }
        }

        public void RefreshPackages()
        {
            _navigationController.RestartGettingUpdates();
        }

        public bool RefreshInProgress
        {
            get { return _taskManager.IsTaskRunning<GetUpdatesTask>(); }
        }

        public void Handle(TaskMessage message)
        {
            if (message.Task is GetUpdatesTask)
            {
                NotifyOfPropertyChange(() => RefreshInProgress);
            }
        }
    }
}
