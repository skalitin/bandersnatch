﻿using System;
using System.Linq;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.Commands;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Samples;
using Quest.Cyclone.Client.Windows.Commons.Tasks;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public interface IDatasheetsFrame
    {
    }

    public class DatasheetsFrameActivatedMessage
    {
        public IDatasheetsFrame Frame { get; set; }    
    }

    public class DatasheetsViewModel : ConductorBase<IDatasheetsFrame>, IHandle<TaskMessage>, IHandle<DatasheetsFrameActivatedMessage>, IRefresh, IDatasheetCommandVisitor
    {
        private readonly ITaskManager _taskManager;
        private readonly IPackageStorage _packageStorage;
        private readonly IEventAggregator _eventAggregator;
        private Instance _selectedInstance;
        private readonly Package _package;

        public DatasheetsViewModel()
        {
            if (Execute.InDesignMode)
            {
                _package = Samples.Package;
            }
        }

        public DatasheetsViewModel(INavigationService navigationService, IPackageStorage packageStorage, ITaskManager taskManager, IEventAggregator eventAggregator, Guid packageId) 
            : base(navigationService)
        {
            _eventAggregator = eventAggregator;
            _taskManager = taskManager;
            _packageStorage = packageStorage;
            _package = _packageStorage.GetPackage(packageId);
            _eventAggregator.Subscribe(this);
        }

        public void Visit(ShowDatasheetsCommand command)
        {
            var model = new DatasheetListViewModel(_taskManager, _eventAggregator, SelectedInstance.Id, command.DatasheetModels);
            Items.Add(model);
        }

        public void Visit(ShowDatasheetContentCommand command)
        {
            var model = new DatasheetContentViewModel(_taskManager, _eventAggregator, SelectedInstance.Id, command.DatasheetContentModel);
            Items.Add(model);
        }

        protected override void OnActivate()
        {
            if (Instances.Count == 1)
            {
                SelectedInstance = Instances.First();
            }
        }

        public override string DisplayName
        {
            get { return Utils.GetStringFromResource("Reports"); }
        }

        public BindableCollection<Instance> Instances
        {
            get { return _package.Instances; }
        }

        public bool ShowInstancesSelector
        {
            get { return Instances.Count() > 1; }
        }

        public Instance SelectedInstance
        {
            get { return _selectedInstance; }
            set
            {
                _selectedInstance = value;
                NotifyOfPropertyChange(() => SelectedInstance);
               LoadRootDatasheets();
            }
        }

        private void LoadRootDatasheets()
        {
            Items.Clear();
            _taskManager.RunTask(new GetDatasheetsTask(Guid.Empty, _selectedInstance.Id, true));
        }

        public bool LoadingInProgress
        {
            get { return _taskManager.IsTaskRunning<DatasheetsTask>(); }
        }

        public void Handle(TaskMessage message)
        {
            var task = message.Task as DatasheetsTask;
            if (task != null)
            {
                NotifyOfPropertyChange(() => LoadingInProgress);

                if (message is TaskFinishedMessage)
                {
                    var command = task.Command;
                    command.AcceptVisitor(this);
                }
            }
        }

        public void RefreshData(bool initial)
        {
            LoadRootDatasheets();
        }

        public void Handle(DatasheetsFrameActivatedMessage message)
        {
            var frameIndex = Items.IndexOf(message.Frame);
            for (var i = Items.Count - 1; i > frameIndex; i--)
            {
                Items.RemoveAt(i);
            }
        }
    }
}
