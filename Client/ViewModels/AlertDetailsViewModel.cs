﻿using System;
using System.Linq;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Samples;
using Quest.Cyclone.Client.Windows.Commons.Tasks;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class AlertDetailsViewModel : ViewModelBase, IHandle<TaskMessage>, IHandle<AlertRemovedMessage>
    {
        private readonly IUIManager _iuiManager;
        private readonly ITaskManager _taskManager;
        private readonly IServerRequestsManager _serverRequestsManager;
        private readonly IAlertStorage _alertStorage;
        private readonly ISettings _settings;
        private readonly Guid _packageId;

        public AlertDetailsViewModel()
        {
            if (Execute.InDesignMode)
            {
                Alert = Samples.Alert;
            }
        }

        public AlertDetailsViewModel(INavigationService navigationService, IUIManager iuiManager, ISettings settings, IServerRequestsManager serverRequestsManager, IAlertStorage alertStorage, ITaskManager taskManager, IEventAggregator eventAggregator, Alert alert, Guid packageId)
            : base(navigationService)
        {
            _iuiManager = iuiManager;
            _taskManager = taskManager;
            _packageId = packageId;
            _serverRequestsManager = serverRequestsManager;
            _alertStorage = alertStorage;
            _settings = settings;

            eventAggregator.Subscribe(this);

            Alert = alert;

            LoadAlertDetails();
        }

        public void Acknowledge()
        {
            var task = new AcknowledgeAlertTask(_settings, _serverRequestsManager, _alertStorage, _packageId, Alert.Id);
            _taskManager.RunTask(task);
        }

        public void RunCustomAction(Guid actionId)
        {
            var task = new RunAlertActionTask(_settings, _serverRequestsManager, _alertStorage, _packageId, Alert.Id, actionId);
            _taskManager.RunTask(task);
        }

        private void LoadAlertDetails()
        {
            if (Alert != null && Alert.Full == null)
            {
                var task = new GetAlertDetailsTask(_alertStorage, _settings, _serverRequestsManager, Alert.Id, _packageId);
                _taskManager.RunTask(task);
            }
        }

        public bool ShowTextCustomFields
        {
            get
            {
                if (Alert != null && Alert.Full != null)
                {
                    return Alert.Full.CustomFields.Any(field => field.Type == CustomFieldType.Text);
                }

                return false;
            }
        }

        public bool ShowHtmlCustomFields
        {
            get
            {
                if (Alert != null && Alert.Full != null)
                {
                    return Alert.Full.CustomFields.Any(field => field.Type == CustomFieldType.HTML);
                }

                return false;
            }
        }

        public Guid PackageId 
        {
            get { return _packageId; }
        }

        public Alert Alert { get; set; }

        public bool OperationInProgress
        {
            get
            {
                return
                    Alert.Brief.State == RunState.InProgress ||
                    _taskManager.IsTaskRunning<GetAlertDetailsTask>(task => task.Uses(Alert)) ||
                    _taskManager.IsTaskRunning<ResolveAlertTask>(task => task.Uses(Alert)) ||
                    _taskManager.IsTaskRunning<AcknowledgeAlertTask>(task => task.Uses(Alert)) ||
                    _taskManager.IsTaskRunning<RunAlertActionTask>(task => task.Uses(Alert));
            }
        }

        public void Handle(TaskMessage message)
        {
            if (message.Task is GetAlertDetailsTask || 
                message.Task is AcknowledgeAlertTask ||
                message.Task is ResolveAlertTask ||
                message.Task is RunAlertActionTask ||
                message.Task is GetUpdatesTask)
            {
                NotifyOfPropertyChange(() => OperationInProgress);
                NotifyOfPropertyChange(() => ShowTextCustomFields);
                NotifyOfPropertyChange(() => ShowHtmlCustomFields);
            }
        }

        public void Handle(AlertRemovedMessage message)
        {
            if (Alert != null && message.AlertId == Alert.Id)
            {
                _iuiManager.ShowToast(Utils.GetStringFromResource("OwnerChangedMessage") + " " + Alert.Brief.Owner);
            }
        }
    }
}
