﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Models;
using Quest.Cyclone.Client.Windows.Commons.Samples;
using Quest.Cyclone.Client.Windows.Commons.Tasks;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class DatasheetListViewModel : Screen, IDatasheetsFrame
    {
        private readonly ITaskManager _taskManager;
        private readonly IEventAggregator _eventAggregator;
        private DatasheetModel _selectedDatasheetModel;
        private readonly Guid _instanceId;

        public DatasheetListViewModel(ITaskManager taskManager, IEventAggregator eventAggregator, Guid instanceId, IEnumerable<DatasheetModel> datasheetModels)
        {
            _taskManager = taskManager;
            _instanceId = instanceId;
            _eventAggregator = eventAggregator;
            DatasheetModels = new BindableCollection<DatasheetModel>(datasheetModels);
        }

        public BindableCollection<DatasheetModel> DatasheetModels { get; set; }

        public DatasheetModel SelectedDatasheetModel
        {
            get { return _selectedDatasheetModel; }
            set
            {
                _selectedDatasheetModel = value;
                NotifyOfPropertyChange(() => SelectedDatasheetModel);

                if (SelectedDatasheetModel != null)
                {
                    if (SelectedDatasheetModel.IsExpandable)
                    {
                        _taskManager.RunTask(new GetDatasheetsTask(SelectedDatasheetModel.Id, _instanceId, true));
                    }
                    if (SelectedDatasheetModel.HasRecords)
                    {
                        _taskManager.RunTask(new GetDatasheetContentTask(SelectedDatasheetModel.Id, _instanceId, new Range(0, 1000), true));
                    }

                    _eventAggregator.Publish(new DatasheetsFrameActivatedMessage() { Frame = this });
                }
            }
        }
    }
}
