﻿using Caliburn.Micro;

using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Tasks;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class LogonViewModel : ViewModelBase, IHandle<TaskMessage>
    {
        private readonly ITaskManager _taskManager;
        private readonly ISettings _settings;

        public LogonViewModel(INavigationService navigationService, ITaskManager taskManager, ISettings settings)
            : base(navigationService)
        {
            _taskManager = taskManager;
            _settings = settings;
        }

        public string ServerName 
        {
            get
            {
                return _settings.ServerName;
            }

            set
            {
                _settings.ServerName = value;
            }
        }

        public bool IsUsingCompanyName
        {
            get
            {
                return _settings.IsUsingCompanyName;
            }

            set
            {
                _settings.IsUsingCompanyName = value;
            }
        }

        public string UserName
        {
            get
            {
                return _settings.UserName;
            }

            set
            {
                _settings.UserName = value;
            }
        }

        public string UserPassword
        {
            get
            {
                return _settings.UserPassword;
            }

            set
            {
                _settings.UserPassword = value;
            }
        }

        public string Version
        {
            get
            {
                return _settings.ApplicationVersion;
            }
        }

        public bool SignInInProgress
        {
            get { return _taskManager.IsTaskRunning<RegisterTask>(); }
        }

        public void SignIn()
        {
            _taskManager.RunTask<RegisterTask>();
        }

        public void Handle(TaskMessage message)
        {
            if (message.Task is RegisterTask)
            {
                NotifyOfPropertyChange(() => SignInInProgress);
            }
        }
    }
}
