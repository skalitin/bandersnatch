﻿using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class AboutViewModel : SettingsViewModel
    {
        private readonly ISettings _settings;

        public AboutViewModel(INavigationService navigationService, ISettings settings)
            : base(navigationService)
        {
            _settings = settings;
        }

        public string Version
        {
            get
            {
                return _settings.ApplicationVersion;
            }
        }
    }
}
