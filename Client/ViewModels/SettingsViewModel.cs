﻿using Caliburn.Micro;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class SettingsViewModel : ViewModelBase
    {
        public SettingsViewModel(INavigationService navigationService)
            : base(navigationService)
        {
        }

        protected void Close()
        {
            if (OnCloseRequested != null)
            {
                OnCloseRequested();
            }
        }

        public System.Action OnCloseRequested;
    }
}
