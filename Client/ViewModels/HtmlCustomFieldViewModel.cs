﻿using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class HtmlCustomFieldViewModel : ViewModelBase
    {
        private CustomField _field;

        public HtmlCustomFieldViewModel(INavigationService navigationService)
            : base(navigationService)
        {
        }

        public CustomField Field
        {
            get { return _field; }
            set
            {
                _field = value;
                NotifyOfPropertyChange(() => Field);
            }
        }
    }
}
