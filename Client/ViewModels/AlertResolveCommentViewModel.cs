﻿using System;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Tasks;

namespace Quest.Cyclone.Client.Windows.ViewModels
{
    public class AlertResolveCommentViewModel : ViewModelBase
    {
        private readonly IServerRequestsManager _serverRequestsManager;
        private readonly IAlertStorage _alertStorage;
        private readonly ISettings _settings;
        private readonly ITaskManager _taskManager;

        public AlertResolveCommentViewModel(INavigationService navigationService, IServerRequestsManager serverRequestsManager, IAlertStorage alertStorage, ISettings settings, ITaskManager taskManager)
            : base(navigationService)
        {
            _serverRequestsManager = serverRequestsManager;
            _alertStorage = alertStorage;
            _settings = settings;
            _taskManager = taskManager;
        }

        public Guid PackageId { get; set; }
        public Guid AlertId { get; set; }
        public string Comment { get; set; }

        public void Resolve()
        {
            var task = new ResolveAlertTask(_settings, _serverRequestsManager, _alertStorage, PackageId, AlertId, Comment);
            _taskManager.RunTask(task);
        }

        // test comment for code review
    }
}
