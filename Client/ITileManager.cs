﻿using System;
using System.Threading.Tasks;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;

namespace Quest.Cyclone.Client.Windows
{
    public interface ITileManager
    {
        Task<bool> PinPackageAsync(Package package);
        Task<bool> UnpinPackageAsync(Package package);
        bool IsPackagePinned(Package package);
        Guid GetPinnedPackageId(string tileId, string arguments);
    }
}