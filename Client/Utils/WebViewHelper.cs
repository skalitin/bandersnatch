﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Quest.Cyclone.Client.Windows
{
    public static class WebViewHelper
    {
        public static readonly DependencyProperty HtmlProperty = DependencyProperty.RegisterAttached(
            "Html", typeof(string), typeof(WebViewHelper), new PropertyMetadata(null, OnHtmlChanged));

        public static string GetHtml(DependencyObject dependencyObject)
        {
            return (string)dependencyObject.GetValue(HtmlProperty);
        }

        public static void SetHtml(DependencyObject dependencyObject, string value)
        {
            dependencyObject.SetValue(HtmlProperty, value);
        }

        private static void OnHtmlChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
        {
            var view = dependencyObject as WebView;

            if (view == null)
                return;

            var html = args.NewValue.ToString();
            view.NavigateToString(html);
        }
    }
}
