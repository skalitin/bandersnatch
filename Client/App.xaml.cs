﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Callisto.Controls;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Storages;
using Quest.Cyclone.Client.Windows.Commons.Tasks;
using Quest.Cyclone.Client.Windows.ViewModels;
using Quest.Cyclone.Client.Windows.Views;

using Windows.ApplicationModel.Activation;
using Windows.System;
using Windows.UI;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Caliburn.Micro;
using Windows.UI.Xaml.Media;

namespace Quest.Cyclone.Client.Windows
{
    public sealed partial class App
    {
        private WinRTContainer _container;

        public App()
        {
            InitializeComponent();
        }

        protected override void Configure()
        {
            _container = new WinRTContainer();
            _container.RegisterWinRTServices();

            _container.Singleton<IUIManager, UIManager>();
            _container.Singleton<ISettings, Settings>();
            _container.Singleton<IStorage, Storage>();
            _container.Singleton<IServerRequestsManager, ServerRequestsManager>();
            _container.Singleton<ITaskManager, TaskManager>();
            _container.Singleton<IPackageStorage, PackageStorage>();
            _container.Singleton<IAlertStorage, AlertStorage>();
            _container.Singleton<IIconDataRestorer, IconDataRestorer>();
            _container.Singleton<INavigationController, NavigationController>();
            _container.Singleton<ITileManager, TileManager>();
        }

        private static bool IsConcrete(Type service)
        {
            var serviceInfo = service.GetTypeInfo();
            return !serviceInfo.IsAbstract && !serviceInfo.IsInterface;
        }

        protected override object GetInstance(Type service, string key)
        {
            var instance = _container.GetInstance(service, key);
            
            // Mimic previous behaviour of WinRT SimpleContainer
            // See http://www.philliphaydon.com/2013/02/windows-store-app-with-caliburn-micro-getting-started-updated/
            if (instance == null && IsConcrete(service))
            {
                _container.RegisterPerRequest(service, key, service);
                instance = _container.GetInstance(service, key);
            }

            if (instance is IHandle)
            {
                IoC.Get<IEventAggregator>().Subscribe(instance);
            }

            return instance;
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            var instances = _container.GetAllInstances(service).ToList();
            foreach (var instance in instances.OfType<IHandle>())
            {
                IoC.Get<IEventAggregator>().Subscribe(instance);
            }

            return instances;
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        protected override void PrepareViewFirst(Frame rootFrame)
        {
            _container.RegisterNavigationService(rootFrame);
            
            // Creating the single instance of these services so that 
            // they will be able to listen for subscribed events.
            IoC.Get<INavigationController>();
            IoC.Get<IPackageStorage>();
            IoC.Get<IAlertStorage>();
            IoC.Get<ITileManager>();
        }

        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            // Normally wouldn't need to do this but need the container to be initialised
            Initialise();

            SettingsPane.GetForCurrentView().CommandsRequested += OnSettingsCommandsRequested;

            var settings = IoC.Get<ISettings>();
            if (settings.IsLoggedOn)
            {
                var packageId = IoC.Get<ITileManager>().GetPinnedPackageId(args.TileId, args.Arguments);
                if (packageId != Guid.Empty)
                {
                    IoC.Get<INavigationService>().UriFor<PackageViewModel>()
                        .WithParam(model => model.PackageId, packageId)
                        .Navigate();

                   //DisplayRootView<PackageView>(PackageId);
                }
                else
                {
                    DisplayRootView<PackagesView>();
                }
            }
            else
            {
                DisplayRootView<LogonView>();
            }
        }

        private void OnSettingsCommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            var settings = IoC.Get<ISettings>();
            if (settings.IsLoggedOn)
            {
                var optionsLabel = Utils.GetStringFromResource("OptionsLabel");
                var optionsCommand = new SettingsCommand("options",
                    optionsLabel, cmd => OpenSettingsFlyout<OptionsViewModel>(optionsLabel));

                args.Request.ApplicationCommands.Add(optionsCommand);
            }

            var aboutLabel = Utils.GetStringFromResource("AboutLabel");
            var aboutCommand = new SettingsCommand("about",
                aboutLabel, cmd => OpenSettingsFlyout<AboutViewModel>(aboutLabel));

            args.Request.ApplicationCommands.Add(aboutCommand);

            var privacyLabel = Utils.GetStringFromResource("PrivacyPolicyLabel");
            var privacyCommand = new SettingsCommand("privacy",
                privacyLabel, cmd => ShowPrivacyPolicyAsync());

            args.Request.ApplicationCommands.Add(privacyCommand);

        }

        private async void ShowPrivacyPolicyAsync()
        {
            var uri = new Uri(@"http://www.quest.com/legal/privacy.aspx");
            await Launcher.LaunchUriAsync(uri);
        }

        private void OpenSettingsFlyout<T>(string header)
        {
            var view = ViewLocator.LocateForModelType(typeof(T), null, null) as FrameworkElement;
            if (view == null)
                return;

            var viewModel = ViewModelLocator.LocateForView(view);
            if (viewModel == null)
                return;

            ViewModelBinder.Bind(viewModel, view, null);

            var settingsFlyout = new SettingsFlyout
            {
                FlyoutWidth = SettingsFlyout.SettingsFlyoutWidth.Narrow,
                HeaderText = header,
                HeaderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0x10, 0x3a, 0x8e)),
                Content = view,
                IsOpen = true
            };

            var settingsViewModel = viewModel as SettingsViewModel;
            if (settingsViewModel != null)
            {
                settingsViewModel.OnCloseRequested += () => { settingsFlyout.IsOpen = false; };
            }

            settingsFlyout.Closed += (s, e) =>
            {
                var deactivator = viewModel as IDeactivate;
                if (deactivator != null)
                {
                    deactivator.Deactivate(true);
                }
            };

            var activator = viewModel as IActivate;
            if (activator != null)
            {
                activator.Activate();
            }            
        }

        protected override void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            base.OnUnhandledException(sender, e);
        }
    }
}
