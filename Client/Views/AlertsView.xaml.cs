﻿// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

using Windows.UI.Xaml;

namespace Quest.Cyclone.Client.Windows.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AlertsView
    {
        public AlertsView()
        {
            this.InitializeComponent();
        }
    }
}
