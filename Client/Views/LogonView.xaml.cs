﻿using System;

using Quest.Cyclone.Client.Windows.Commons;

using Windows.UI.Xaml;

namespace Quest.Cyclone.Client.Windows.Views
{
    public sealed partial class LogonView
    {
        public LogonView()
        {
            InitializeComponent();
            Loaded += OnLoaded;
            IsUsingCompanyName.Click += IsUsingCompanyNameOnClick;
            UserPassword.GotFocus += UserPasswordOnGotFocus;
            UserPassword.LostFocus += UserPasswordOnLostFocus;
        }

        private void UserPasswordOnLostFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            UpdateUserPasswordLabel();
        }

        private void UpdateUserPasswordLabel()
        {
            var passwordEmpty = string.IsNullOrEmpty(UserPassword.Password);
            UserPasswordWatermark.Opacity = passwordEmpty ? 100 : 0;
            UserPassword.Opacity = passwordEmpty ? 0 : 100;
        }

        private void UserPasswordOnGotFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            UserPasswordWatermark.Opacity = 0;
            UserPassword.Opacity = 100;
        }

        private void IsUsingCompanyNameOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            UpdateServerNameLabel();
        }

        private void UpdateServerNameLabel()
        {
            if (IsUsingCompanyName.IsChecked != null)
            {
                ServerName.WatermarkText =
                    Utils.GetStringFromResource(IsUsingCompanyName.IsChecked.Value
                                                    ? "CompanyNameLabel/WatermarkText"
                                                    : "ServerNameLabel/WatermarkText");
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            UpdateServerNameLabel();
            UpdateUserPasswordLabel();

            // Hack: server name field should not be focused on start
            ServerName.Visibility = Visibility.Collapsed;
            ServerName.Visibility = Visibility.Visible;
        }
    }
}
