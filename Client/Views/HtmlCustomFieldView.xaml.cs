﻿using System;
using Windows.UI.Xaml.Input;

namespace Quest.Cyclone.Client.Windows.Views
{
    public sealed partial class HtmlCustomFieldView
    {
        public HtmlCustomFieldView()
        {
            InitializeComponent();
        }

        private void UIElement_OnTapped(object sender, TappedRoutedEventArgs e)
        {
            if (OnCloseRequested != null)
            {
                OnCloseRequested();
            }
        }
        public Action OnCloseRequested;
    }
}
