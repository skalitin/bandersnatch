﻿using System;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.ViewModels;
using Windows.UI.ApplicationSettings;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Navigation;

namespace Quest.Cyclone.Client.Windows.Views
{
    public sealed partial class PackagesView
    {
        public PackagesView()
        {
            InitializeComponent();
            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            var viewModel = DataContext as PackagesViewModel;
            if (viewModel != null)
            {
                viewModel.PropertyChanged += (temp, args) =>
                {
                    if (args.PropertyName == Utils.GetPropertyName(() => viewModel.SelectedPackage))
                    {
                        if (viewModel.SelectedPackage != null)
                        {
                            BottomAppBar.IsOpen = true;
                        }
                    }
                };
            }
        }
    }
}
