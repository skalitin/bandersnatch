﻿using System;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.ViewModels;
using Windows.Foundation;
using Windows.UI.ApplicationSettings;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media.Animation;

namespace Quest.Cyclone.Client.Windows.Views
{
    public sealed partial class AlertDetailsView
    {
        // need to save references to views to prevent closing by system
        // static - workaround for bug with crashing after fast closing parent window (very hard to confirm)
        // looks like same problem - http://social.msdn.microsoft.com/Forums/en-US/winappswithnativecode/thread/11fa65e7-90b7-41f5-9884-80064ec6e2d8/
        private static Popup _actionPanel;
        private static Popup _resolveDialog;
        private static Popup _htmlCustomFieldDialog;

        public AlertDetailsView()
        {
            InitializeComponent();

            Resolve.Click += ResolveOnClick;
            Actions.Click += ActionsOnClick;
        }

        public static Rect GetElementRect(FrameworkElement element)
        {
            var buttonTransform = element.TransformToVisual(null);
            var point = buttonTransform.TransformPoint(new Point());

            return new Rect(point, new Size(element.ActualWidth, element.ActualHeight));
        }

        private void ShowResolveDialog()
        {
            var currentWindow = Window.Current.CoreWindow;
            var model = (AlertDetailsViewModel)DataContext;
            var view = (AlertResolveCommentView)(ViewLocator.LocateForModelType(typeof(AlertResolveCommentViewModel), null, null) as FrameworkElement);
            if (view == null)
            {
                return;
            }

            view.Height = currentWindow.Bounds.Height;
            view.Width = currentWindow.Bounds.Width;
            view.OnCloseRequested += () => _resolveDialog.IsOpen = false;

            var viewModel = (AlertResolveCommentViewModel)ViewModelLocator.LocateForView(view);
            if (viewModel == null)
            {
                return;
            }

            viewModel.PackageId = model.PackageId;
            viewModel.AlertId = model.Alert.Id;

            ViewModelBinder.Bind(viewModel, view, null);

            _resolveDialog = new Popup();
            _resolveDialog.Closed += ResolveDialogOnClosed;
            _resolveDialog.Child = view;
            _resolveDialog.IsLightDismissEnabled = true;
            _resolveDialog.Visibility = Visibility.Visible;
            _resolveDialog.SetValue(Canvas.TopProperty, 0);
            _resolveDialog.IsOpen = true;
        }

        private void ResolveDialogOnClosed(object sender, object o)
        {
            _resolveDialog.Child = null;
        }

        private async void ShowMenu(FrameworkElement sender)
        {
            var model = (AlertDetailsViewModel)DataContext;
            var alert = model.Alert;
            if (alert == null || alert.Full == null)
            {
                return;
            }

            var menu = new PopupMenu();
            for (var num = 0; num < alert.Full.Actions.Length && num < Constants.MaximumAlertActionsCount; num++ )
            {
                var local = num;
                menu.Commands.Add(new UICommand(alert.Full.Actions[num].Title,
                delegate
                {
                    ShowActionPanel(model.PackageId, alert, alert.Full.Actions[local]);
                }));
            }

            await menu.ShowForSelectionAsync(GetElementRect(sender));
        }

        private void ShowActionPanel(Guid packageId, Alert alert, ActionDefinition action)
        {
            var currentWindow = Window.Current.CoreWindow;
            
            var view = ViewLocator.LocateForModelType(typeof(AlertActionViewModel), null, null) as FrameworkElement;
            if (view == null)
            {
                return;
            }

            view.Height = currentWindow.Bounds.Height;
            view.Width = 330;

            var viewModel = (AlertActionViewModel)ViewModelLocator.LocateForView(view);
            if (viewModel == null)
            {
                return;
            }

            viewModel.PackageId = packageId;
            viewModel.Alert = alert;
            viewModel.Action = action;
            
            ViewModelBinder.Bind(viewModel, view, null);

            _actionPanel = new Popup();
            _actionPanel.HorizontalOffset = currentWindow.Bounds.Width - view.Width;
            _actionPanel.Closed += OnActionPanelClosed;
            _actionPanel.Child = view;
            _actionPanel.ChildTransitions = new TransitionCollection();
            _actionPanel.ChildTransitions.Add(new PaneThemeTransition { Edge = (SettingsPane.Edge == SettingsEdgeLocation.Right) ? EdgeTransitionLocation.Right : EdgeTransitionLocation.Left });
            _actionPanel.IsLightDismissEnabled = true;
            _actionPanel.Visibility = Visibility.Visible;
            _actionPanel.SetValue(Canvas.TopProperty, 0);
            _actionPanel.IsOpen = true;
        }

        private void OnActionPanelClosed(object sender, object e)
        {
            _actionPanel.Child = null;
        }

        private void ResolveOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            ShowResolveDialog();
        }

        private void ActionsOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            ShowMenu((FrameworkElement)sender);
        }

        private void ViewHtmlField_OnClick(object sender, RoutedEventArgs e)
        {
            var field = (CustomField)((HyperlinkButton) sender).DataContext;

            ShowHtmlCustomField(field);
        }

        private void ShowHtmlCustomField(CustomField field)
        {
            var currentWindow = Window.Current.CoreWindow;

            var view = (HtmlCustomFieldView)(ViewLocator.LocateForModelType(typeof(HtmlCustomFieldViewModel), null, null) as FrameworkElement);
            if (view == null)
            {
                return;
            }

            view.Height = currentWindow.Bounds.Height;
            view.Width = currentWindow.Bounds.Width;
            view.OnCloseRequested += () => _htmlCustomFieldDialog.IsOpen = false; 

            var viewModel = (HtmlCustomFieldViewModel)ViewModelLocator.LocateForView(view);
            if (viewModel == null)
            {
                return;
            }

            viewModel.Field = field;

            ViewModelBinder.Bind(viewModel, view, null);
            
            _htmlCustomFieldDialog = new Popup();
            _htmlCustomFieldDialog.HorizontalOffset = (currentWindow.Bounds.Width / 2) - (view.Width / 2);
            _htmlCustomFieldDialog.VerticalOffset = (currentWindow.Bounds.Height / 2) - (view.Height / 2);
            _htmlCustomFieldDialog.Closed += HtmlCustomFieldDialogOnClosed;
            _htmlCustomFieldDialog.Child = view;
            // TODO Transition doesn't affect WebView and it looks ugly
            //_htmlCustomFieldDialog.ChildTransitions = new TransitionCollection();
            //_htmlCustomFieldDialog.ChildTransitions.Add(new PaneThemeTransition { Edge = (SettingsPane.Edge == SettingsEdgeLocation.Right) ? EdgeTransitionLocation.Right : EdgeTransitionLocation.Left });
            _htmlCustomFieldDialog.IsLightDismissEnabled = true;
            _htmlCustomFieldDialog.Visibility = Visibility.Visible;
            _htmlCustomFieldDialog.SetValue(Canvas.TopProperty, 0);
            _htmlCustomFieldDialog.IsOpen = true;
        }

        private void HtmlCustomFieldDialogOnClosed(object sender, object o)
        {
            _htmlCustomFieldDialog.Child = null;
        }
    }
}
