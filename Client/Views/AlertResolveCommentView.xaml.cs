﻿using System;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace Quest.Cyclone.Client.Windows.Views
{
    public sealed partial class AlertResolveCommentView
    {
        public AlertResolveCommentView()
        {
            InitializeComponent();

            Resolve.Click += ResolveClick;
        }

        private void ResolveClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (OnCloseRequested != null)
            {
                OnCloseRequested();
            }
        }

        private void UIElement_OnTapped(object sender, TappedRoutedEventArgs e)
        {
            // Workaround to hide the touch keyboard
            Comment.Visibility = Visibility.Collapsed;
            Comment.Visibility = Visibility.Visible;

            if (OnCloseRequested == null)
            {
                return;
            }

            // Close dialog only if the touch keyboard is hidden
            var inputPane = InputPane.GetForCurrentView();
            if (inputPane != null && inputPane.OccludedRect.Width == 0 && inputPane.OccludedRect.Height == 0)
            {
                OnCloseRequested();
            }
        }

        public Action OnCloseRequested;
    }
}
