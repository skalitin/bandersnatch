﻿using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Tasks;
using Quest.Cyclone.Client.Windows.ViewModels;
using Quest.Cyclone.Client.Windows.Views;

namespace Quest.Cyclone.Client.Windows
{
    public class NavigationController : INavigationController, IHandle<TaskFinishedMessage>, IHandle<SettingMessage>
    {
        private readonly ITaskManager _taskManager;
        private readonly INavigationService _navigationService;
        private readonly ISettings _settings;

        public NavigationController(ITaskManager taskManager, INavigationService navigationService, ISettings settings)
        {
            _taskManager = taskManager;
            _navigationService = navigationService;
            _settings = settings;
        }

        public void SignOut()
        {
            _taskManager.RunTask<UnregisterTask>();
        }

        public void RestartGettingUpdates()
        {
            _taskManager.RunPeriodicTask<GetUpdatesTask>(_settings.UpdateInterval);
        }

        public void Handle(TaskFinishedMessage message)
        {
            if (message.Task is RegisterTask)
            {
                if (_settings.IsLoggedOn && _settings.DeviceStatus == DeviceStatusType.Approved)
                {
                    _navigationService.NavigateToViewModel<PackagesViewModel>();
                }
            }

            if (message.Task is GetPackagesTask)
            {
                RestartGettingUpdates();
            }

            if (message.Task is UnregisterTask)
            {
                if (!_settings.IsLoggedOn)
                {
                    _navigationService.NavigateToViewModel<LogonViewModel>();
                }
            }
        }

        public void Handle(SettingMessage settingMessage)
        {
            if (settingMessage.Is(() => _settings.DeviceStatus))
            {
                if (_settings.DeviceStatus == DeviceStatusType.NotApproved && settingMessage.IsChanged)
                {
                    _navigationService.NavigateToViewModel<DeviceNotApprovedViewModel>();
                }

                if (_settings.DeviceStatus == DeviceStatusType.Approved && settingMessage.IsChanged && _settings.IsLoggedOn)
                {
                    _navigationService.NavigateToViewModel<PackagesViewModel>();
                }

                if (_settings.DeviceStatus == DeviceStatusType.Blocked)
                {
                    Utils.ShowError(_settings.DeviceStatus.Message);

                    if (_settings.IsLoggedOn)
                    {
                        SignOut();    
                    }
                }
            }
        }
    }
}
