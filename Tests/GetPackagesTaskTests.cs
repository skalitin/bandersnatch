﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using Moq;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Storages;
using Quest.Cyclone.Client.Windows.Commons.Tasks;
using Quest.Cyclone.Client.Windows.Tests.Stubs;

namespace Quest.Cyclone.Client.Windows.Tests
{
    [TestClass]
    public class GetPackagesTaskTests : BaseTests
    {
        private IPackageStorage _packageStorage;
        private GetPackagesTask _task;
        private static Mock<IServerRequestsManager> _mockServerRequestsManager;
        private static IServerRequestsManager _serverRequestsManager;
        private StubIconDataRestorer _stubIconDataRestorer;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _mockServerRequestsManager = new Mock<IServerRequestsManager>();
            _serverRequestsManager = _mockServerRequestsManager.Object;
        }

        [TestInitialize]
        public override void SetUp()
        {
            base.SetUp();
            _packageStorage = new PackageStorage(_settings, _eventAggregator);
            _stubIconDataRestorer = new StubIconDataRestorer();
            _task = new GetPackagesTask(_settings, _serverRequestsManager, _packageStorage, _stubIconDataRestorer);
        }

        [TestMethod]
        public void UpdatingPackageList()
        {
            var package1 = new Package() { Id = Guid.NewGuid() };
            var package2 = new Package() { Id = Guid.NewGuid() };
            var packagesResponse = new PackagesResponse()
            {
                Packages = { package1, package2 }
            };
            _mockServerRequestsManager.Setup(o => o.GetPackages()).Returns(packagesResponse);

            _task.Execute();
            CollectionAssert.AreEqual(new[]{ package1, package2 }, _packageStorage.Packages);
        }

        [TestMethod]
        public void UpdatingSyncId()
        {
            var packagesResponse = new PackagesResponse() { SyncId = 42 };
            _mockServerRequestsManager.Setup(o => o.GetPackages()).Returns(packagesResponse);

            _task.Execute();
            Assert.AreEqual(42, _settings.SyncId);
        }

        [TestMethod]
        public void RestoringPackageIcons()
        {
            var package = new Package();
            var packagesResponse = new PackagesResponse() { Packages = { package } };
            _mockServerRequestsManager.Setup(o => o.GetPackages()).Returns(packagesResponse);

            _task.Execute();
            CollectionAssert.AreEqual(_stubIconDataRestorer.Data, package.Icon.Data);
        }
    }
}


