﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Samples;
using Quest.Cyclone.Client.Windows.Commons.Storages;
using Quest.Cyclone.Client.Windows.Commons.Tasks;
using Quest.Cyclone.Client.Windows.Tests.Stubs;

namespace Quest.Cyclone.Client.Windows.Tests
{
    [TestClass]
    public class PackageStorageTests : BaseTests, IHandle<PackageMessage>
    {
        private PackageStorage _packageStorage;
        private Package _package1;
        private Package _package2;
        private List<PackageMessage> _messages = new List<PackageMessage>();

        [TestInitialize]
        public override void SetUp()
        {
            base.SetUp();

            _package1 = Samples.Package;
            _package2 = DeepClone(_package1);
            _package2.Id = Guid.NewGuid();

            _packageStorage = new PackageStorage(_settings, _eventAggregator);
            _messages.Clear();
        }

        [TestMethod]
        public void AddingSinglePackage()
        {
            _packageStorage.AddPackage(_package1);
            Assert.AreEqual(_package1, _packageStorage.Packages.Single());
        }

        [TestMethod]
        public void AddingPackages()
        {
            var packages = new[] {_package1, _package2};
            _packageStorage.AddPackages(packages);
            
            CollectionAssert.AreEqual(packages, _packageStorage.Packages);
        }

        [TestMethod]
        public void SettingPackages()
        {
            var packages = new[] { _package1, _package2 };
            _packageStorage.SetPackages(packages);

            CollectionAssert.AreEqual(packages, _packageStorage.Packages);
        }

        [TestMethod]
        public void RemovingPackage()
        {
            var packages = new[] { _package1, _package2 };
            _packageStorage.AddPackages(packages);

            _packageStorage.RemovePackage(_package2.Id);
            Assert.AreEqual(_package1, _packageStorage.Packages.Single());
        }

        [TestMethod]
        public void RemovingPackages()
        {
            var packages = new[] { _package1, _package2 };
            _packageStorage.AddPackages(packages);

            _packageStorage.RemovePackages(new[] {_package2.Id});
            Assert.AreEqual(_package1, _packageStorage.Packages.Single());
        }

        [TestMethod]
        public void RemovingAllPackages()
        {
            var packages = new[] { _package1, _package2 };
            _packageStorage.AddPackages(packages);

            _packageStorage.RemovePackages();
            Assert.IsTrue(_packageStorage.Packages.Count == 0);
        }

        [TestMethod]
        public void AddingSinglePackagePublishesMessage()
        {
            _eventAggregator.Subscribe(this);
            
            _packageStorage.AddPackage(_package1);
            Assert.IsTrue(_messages.Count == 1);
            Assert.IsTrue(_messages.All(each => each is PackageCountChangedMessage));
        }

        [TestMethod]
        public void AddingPackagesPublishesMessage()
        {
            _eventAggregator.Subscribe(this);

            var packages = new[] { _package1, _package2 };
            _packageStorage.AddPackages(packages);
            Assert.IsTrue(_messages.Count == 1);
            Assert.IsTrue(_messages.All(each => each is PackageCountChangedMessage));
        }

        [TestMethod]
        public void SettingPackagesPublishesMessage()
        {
            _eventAggregator.Subscribe(this);

            var packages = new[] { _package1, _package2 };
            _packageStorage.SetPackages(packages);
            Assert.IsTrue(_messages.Count == 1);
            Assert.IsTrue(_messages.All(each => each is PackageCountChangedMessage));
        }

        [TestMethod]
        public void RemovingPackagePublishesMessage()
        {
            var packages = new[] { _package1, _package2 };
            _packageStorage.AddPackages(packages);

            _eventAggregator.Subscribe(this);
            _packageStorage.RemovePackage(_package2.Id);

            Assert.IsTrue(_messages.Count == 1);
            Assert.IsTrue(_messages.All(each => each is PackageCountChangedMessage));
        }

        [TestMethod]
        public void RemovingPackagesPublishesMessage()
        {
            var packages = new[] { _package1, _package2 };
            _packageStorage.AddPackages(packages);

            _eventAggregator.Subscribe(this);
            _packageStorage.RemovePackages(new[] {_package2.Id});

            Assert.IsTrue(_messages.Count == 1);
            Assert.IsTrue(_messages.All(each => each is PackageCountChangedMessage));
        }

        [TestMethod]
        public void RemovingAllPackagesPublishesMessage()
        {
            var packages = new[] { _package1, _package2 };
            _packageStorage.AddPackages(packages);

            _eventAggregator.Subscribe(this);
            _packageStorage.RemovePackages();

            Assert.IsTrue(_messages.Count == 1);
            Assert.IsTrue(_messages.All(each => each is PackageCountChangedMessage));
        }

        [TestMethod]
        public void UpdatingPackageBadgePublishesMessage()
        {
            _package1.Badge = 10;
            _packageStorage.AddPackage(_package1);
            _eventAggregator.Subscribe(this);

            _packageStorage.UpdatePackageBadgeCount(_package1.Id, 42);
            
            var message = _messages.Single() as PackageBadgeChangedMessage;
            Assert.AreEqual(_package1.Id, message.PackageId);
        }

        [TestMethod]
        public void UpdatingPackageBadgeToTheSameDoesntPublishMessage()
        {
            _package1.Badge = 42;
            _packageStorage.AddPackage(_package1);
            _eventAggregator.Subscribe(this);

            _packageStorage.UpdatePackageBadgeCount(_package1.Id, 42);
            Assert.IsTrue(_messages.Count == 0);
        }
        
        [TestMethod]
        public void GetPackageById()
        {
            _packageStorage.AddPackage(_package1);
            Assert.AreEqual(_package1, _packageStorage.GetPackage(_package1.Id));
            Assert.IsNull(_packageStorage.GetPackage(_package2.Id));
        }

        public void Handle(PackageMessage message)
        {
            _messages.Add(message);
        }
    }
}


