﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Samples;
using Quest.Cyclone.Client.Windows.Commons.Storages;
using Quest.Cyclone.Client.Windows.Commons.Tasks;
using Quest.Cyclone.Client.Windows.Tests.Stubs;

namespace Quest.Cyclone.Client.Windows.Tests
{
    [TestClass]
    public class AlertStorageTests : BaseTests, IHandle<NewAlertsChangedMessage>
    {
        private AlertStorage _alertStorage;
        private Guid _packageId1;
        private Guid _packageId2;
        private Alert _alert1;
        private Alert _alert2;
        private List<NewAlertsChangedMessage> _messages;

        [TestInitialize]
        public override void SetUp()
        {
            base.SetUp();

            _alertStorage = new AlertStorage(_settings, _eventAggregator);
            _packageId1 = Guid.NewGuid();
            _packageId2 = Guid.NewGuid();
            
            _alert1 = Samples.Alert;
            _alert2 = DeepClone(Samples.Alert);
            _alert2.Id = Guid.NewGuid();

            _messages = new List<NewAlertsChangedMessage>();
        }

        [TestMethod]
        public void SettingAlerts()
        {
            var alerts = new[] { _alert1, _alert2 };
            _alertStorage.SetAlerts(_packageId1, alerts);

            CollectionAssert.AreEqual(alerts, _alertStorage.GetAlerts(_packageId1));
            CollectionAssert.AreEqual(new Alert[]{}, _alertStorage.GetAlerts(_packageId2));
        }

        [TestMethod]
        public void GettingAlertsInStatus()
        {
            _alert1.Brief.Status = AlertStatus.Acknowledged;
            _alert2.Brief.Status = AlertStatus.Resolved;
            var alerts = new[] { _alert1, _alert2 };
            _alertStorage.SetAlerts(_packageId1, alerts);
            _alertStorage.SetAlerts(_packageId2, alerts);

            var result = _alertStorage.GetAlerts(AlertStatus.Acknowledged);
            CollectionAssert.AreEqual(new[] { _alert1, _alert1 }, result.ToArray());
        }

        [TestMethod]
        public void GettingAlertsInStatusByPackage()
        {
            _alert1.Brief.Status = AlertStatus.Acknowledged;
            _alert2.Brief.Status = AlertStatus.Resolved;
            var alerts = new[] { _alert1, _alert2 };
            _alertStorage.SetAlerts(_packageId1, alerts);

            CollectionAssert.AreEqual(new[]{ _alert1 }, _alertStorage.GetAlerts(_packageId1, AlertStatus.Acknowledged));
            Assert.IsTrue(_alertStorage.HasAlerts(_packageId1, AlertStatus.Acknowledged));
            Assert.IsTrue(_alertStorage.HasAlerts(_packageId1, AlertStatus.Resolved));
            Assert.IsFalse(_alertStorage.HasAlerts(_packageId1, AlertStatus.New));
        }

        [TestMethod]
        public void RemovingAlertsByAlertId()
        {
            var alerts = new[] { _alert1, _alert2 };
            _alertStorage.SetAlerts(_packageId1, alerts);

            _alertStorage.RemoveAlerts(_packageId1, new[]{new AlertId {Id= _alert2.Id}});
            Assert.IsTrue(_alertStorage.GetAlerts(_packageId1).Contains(_alert1));
            Assert.IsFalse(_alertStorage.GetAlerts(_packageId1).Contains(_alert2));
        }

        [TestMethod]
        public void RemovingAlertsByStatus()
        {
            _alert1.Brief.Status = AlertStatus.Acknowledged;
            _alert2.Brief.Status = AlertStatus.Resolved;
            var alerts = new[] { _alert1, _alert2 };
            _alertStorage.SetAlerts(_packageId1, alerts);

            _alertStorage.RemoveAlerts(_packageId1, AlertStatus.Acknowledged);
            Assert.IsTrue(_alertStorage.GetAlerts(_packageId1).Contains(_alert2));
            Assert.IsFalse(_alertStorage.GetAlerts(_packageId1).Contains(_alert1));
        }

        [TestMethod]
        public void UpdatingAlertBasicProperties()
        {
            _alertStorage.SetAlerts(_packageId1, new[]{ _alert1 });

            var alert = DeepClone(_alert1);
            alert.Brief.Owner = "New Owner";
            alert.Brief.Status = AlertStatus.Resolved;

            _alertStorage.UpdateAlert(_packageId1, alert);
            Assert.AreEqual("New Owner", _alert1.Brief.Owner);
            Assert.AreEqual(AlertStatus.Resolved, _alert1.Brief.Status);
        }

        [TestMethod]
        public void UpdatingAlertHistory()
        {
            var history = new[]
            {
                new HistoryItem()
                {
                    Type = HistoryItemType.ActionStarted,
                    Time = DateTime.Now.Subtract(TimeSpan.FromMinutes(5)).Ticks,
                },
                new HistoryItem()
                {
                    Type = HistoryItemType.Created,
                    Time = DateTime.Now.Subtract(TimeSpan.FromMinutes(10)).Ticks,
                },
            };
            _alert1.HistoryItems = history.ToList();
            _alertStorage.SetAlerts(_packageId1, new[] { _alert1 });
            Assert.AreEqual(HistoryItemType.ActionStarted, _alert1.HistoryItems.First().Type);

            var alert = DeepClone(_alert1);
            var newHistoryItem = new HistoryItem()
            {
                Type = HistoryItemType.ActionFinished,
                Time = DateTime.Now.Subtract(TimeSpan.FromMinutes(1)).Ticks,
            };
            alert.HistoryItems.Add(newHistoryItem);

            _alertStorage.UpdateAlert(_packageId1, alert);
            Assert.AreEqual(3, _alert1.HistoryItems.Count);
            Assert.AreEqual(HistoryItemType.ActionFinished, _alert1.HistoryItems.First().Type);
        }

        [TestMethod]
        public void SettingNewAlertsPublishesMessage()
        {
            _eventAggregator.Subscribe(this);
            
            _alert1.Brief.Status = AlertStatus.Acknowledged;
            _alertStorage.SetAlerts(_packageId1, new[] { _alert1 });            
            Assert.AreEqual(0, _messages.Count);

            _alert1.Brief.Status = AlertStatus.New;
            _alertStorage.SetAlerts(_packageId1, new[] { _alert1 });            
            
            Assert.AreEqual(1, _messages.Count);
            Assert.AreEqual(_packageId1, _messages.First().PackageId);
        }

        [TestMethod]
        public void RemovingNewAlertsPublishesMessage()
        {
            _alert1.Brief.Status = AlertStatus.Acknowledged;
            _alertStorage.SetAlerts(_packageId1, new[]{ _alert1 });

            _eventAggregator.Subscribe(this);

            _alertStorage.RemoveAlerts();
            Assert.AreEqual(0, _messages.Count);

            _alert1.Brief.Status = AlertStatus.New;
            _alertStorage.SetAlerts(_packageId1, new[] { _alert1 });
            _alert2.Brief.Status = AlertStatus.Acknowledged;
            _alertStorage.SetAlerts(_packageId2, new[] { _alert2 });
            _messages.Clear();

            _alertStorage.RemoveAlerts();
            Assert.AreEqual(1, _messages.Count);
            Assert.AreEqual(_packageId1, _messages.First().PackageId);
        }

        [TestMethod]
        public void RemovingNewAlertsByStatusPublishesMessage()
        {
            _alert1.Brief.Status = AlertStatus.New;
            _alert2.Brief.Status = AlertStatus.Acknowledged;
            var alerts = new[] { _alert1, _alert2 };
            _alertStorage.SetAlerts(_packageId1, alerts);

            _eventAggregator.Subscribe(this);

            _alertStorage.RemoveAlerts(_packageId1, AlertStatus.Acknowledged);
            Assert.AreEqual(0, _messages.Count);

            _alertStorage.RemoveAlerts(_packageId1, AlertStatus.New);
            Assert.AreEqual(1, _messages.Count);
            Assert.AreEqual(_packageId1, _messages.First().PackageId);
        }

        [TestMethod]
        public void RemovingNewAlertsByIdPublishesMessage()
        {
            _alert1.Brief.Status = AlertStatus.New;
            _alert2.Brief.Status = AlertStatus.Acknowledged;
            var alerts = new[] { _alert1, _alert2 };
            _alertStorage.SetAlerts(_packageId1, alerts);

            _eventAggregator.Subscribe(this);

            _alertStorage.RemoveAlerts(_packageId1, new[]{new AlertId{Id = _alert2.Id}});
            Assert.AreEqual(0, _messages.Count);

            _alertStorage.RemoveAlerts(_packageId1, new[] { new AlertId { Id = _alert1.Id } });
            Assert.AreEqual(1, _messages.Count);
            Assert.AreEqual(_packageId1, _messages.First().PackageId);
        }

        [TestMethod]
        public void UpdatingNewAlertPublishesMessage()
        {
            _alert1.Brief.Status = AlertStatus.New;
            _alertStorage.SetAlerts(_packageId1, new[] { _alert1  });

            _eventAggregator.Subscribe(this);

            var alert = DeepClone(_alert1);
            alert.Brief.Status = AlertStatus.Resolved;

            _alertStorage.UpdateAlert(_packageId1, alert);
            Assert.AreEqual(1, _messages.Count);
            Assert.AreEqual(_packageId1, _messages.First().PackageId);
        }

        [TestMethod]
        public void UpdatingAlertToNewOnePublishesMessage()
        {
            _alert1.Brief.Status = AlertStatus.Acknowledged;
            _alertStorage.SetAlerts(_packageId1, new[] { _alert1 });

            _eventAggregator.Subscribe(this);

            var alert = DeepClone(_alert1);
            alert.Brief.Status = AlertStatus.New;

            _alertStorage.UpdateAlert(_packageId1, alert);
            Assert.AreEqual(1, _messages.Count);
            Assert.AreEqual(_packageId1, _messages.First().PackageId);
        }

        public void Handle(NewAlertsChangedMessage message)
        {
            _messages.Add(message);
        }
    }
}


