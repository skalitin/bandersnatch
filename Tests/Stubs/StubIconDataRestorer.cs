﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Tests.Stubs
{
    class StubIconDataRestorer : IIconDataRestorer
    {
        public Icon RestoreIconData(Icon icon)
        {
            if (icon != null)
            {
                icon.Data = Data;
                return icon;
            }

            return new Icon { Data = Data };
        }

        public Task<Icon> RestoreIconDataAsync(Icon icon)
        {
            var task = Task.Run(() => RestoreIconData(icon));
            task.Wait();

            return task;
        }

        public byte[] Data
        {
            get { return new byte[] {1, 2, 3, 4, 5}; }
        }
    }
}
