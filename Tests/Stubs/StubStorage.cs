﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Tests.Stubs
{
    class StubStorage : IStorage
    {
        private readonly Dictionary<string, object> _data = new Dictionary<string, object>();
        
        public void SaveSetting<T>(string name, T value)
        {
            _data[name] = value;
        }

        public T GetSetting<T>(string name)
        {
            object value;
            if (_data.TryGetValue(name, out value))
            {
                return (T)value;
            }

            return default(T);
        }
    }
}
