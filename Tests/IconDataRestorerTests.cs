﻿using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using Moq;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Tests
{
    [TestClass]
    public class IconDataRestorerTests : BaseTests
    {
        private IconDataRestorer _iconDataRestorer;
        private Mock<IServerRequestsManager> _mockServerRequestsManager;

        [TestInitialize]
        public override void SetUp()
        {
            base.SetUp();

            _mockServerRequestsManager = new Mock<IServerRequestsManager>();
            _iconDataRestorer = new IconDataRestorer(_mockServerRequestsManager.Object);
        }

        [TestMethod]
        public void RestoringIconData()
        {
            var iconData = new byte[] {1, 2, 3};
            var icon = new Icon() { Id = "icon_id" };

            _mockServerRequestsManager
                .Setup(o => o.GetImageData("icon_id"))
                .Returns(iconData);

            var restoredIcon = _iconDataRestorer.RestoreIconData(icon);
            Assert.AreEqual(iconData, restoredIcon.Data);
            Assert.AreEqual(iconData, icon.Data);
        }

        [TestMethod]
        public void CachingIconData()
        {
            var iconData = new byte[] { 1, 2, 3 };
            var icon = new Icon() { Id = "icon_id" };

            _mockServerRequestsManager
                .Setup(o => o.GetImageData(icon.Id))
                .Returns(iconData);

            _iconDataRestorer.RestoreIconData(new Icon() { Id = "icon_id" });
            _iconDataRestorer.RestoreIconData(new Icon() { Id = "icon_id" });
            _mockServerRequestsManager.Verify(o => o.GetImageData("icon_id"), Times.Once());
        }

        [TestMethod]
        public void IgnoresNullIcon()
        {
            var result = _iconDataRestorer.RestoreIconData(null);
            Assert.IsNull(result);
        }

        [TestMethod]
        public void IgnoresNullIconId()
        {
            var icon = new Icon();
            var result = _iconDataRestorer.RestoreIconData(icon);
            Assert.IsNull(result.Data);

            icon.Id = "";
            result = _iconDataRestorer.RestoreIconData(icon);
            Assert.IsNull(result.Data);
        }
    }
}


