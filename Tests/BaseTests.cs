﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Caliburn.Micro;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using Quest.Cyclone.Client.Windows.Commons;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Storages;
using Quest.Cyclone.Client.Windows.Commons.Tasks;
using Quest.Cyclone.Client.Windows.Tests.Stubs;

namespace Quest.Cyclone.Client.Windows.Tests
{
    public abstract class BaseTests
    {
        protected ISettings _settings;
        protected IEventAggregator _eventAggregator;
        private IStorage _stubStorage;

        public virtual void SetUp()
        {
            _eventAggregator = new EventAggregator();
            _stubStorage = new StubStorage();
            _settings = new Settings(_stubStorage, _eventAggregator);
        }

        protected T DeepClone<T>(T obj)
        {
            using (var stream = new MemoryStream())
            {
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stream, obj);
                stream.Position = 0;

                return (T)serializer.Deserialize(stream);
            }
        }
    }
}
