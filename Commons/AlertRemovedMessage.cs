﻿using System;

namespace Quest.Cyclone.Client.Windows.Commons
{
    public class AlertRemovedMessage
    {
        public AlertRemovedMessage(Guid alertId)
        {
            AlertId = alertId;
        }

        public Guid AlertId { get; set; }
    }
}
