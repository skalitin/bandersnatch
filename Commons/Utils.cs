﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Xml.Linq;
using Caliburn.Micro;

using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Windows.UI.Popups;
using Action = System.Action;

namespace Quest.Cyclone.Client.Windows.Commons
{
    public class Utils
    {
        public class HttpError
        {
            public HttpError()
            {
                Message = String.Empty;
                StatusCode = Constants.NoStatusCode;
            }
            public string Message { get; set; }
            public int StatusCode { get; set; }
        }

        public static T GetPropertyValue<T>(Expression<Func<T>> propertyId)
        {
            return propertyId.Compile()();
        }

        public static string GetPropertyName<T>(Expression<Func<T>> propertyId)
        {
            return ((MemberExpression)(propertyId.Body)).Member.Name;
        }

        public static DateTime GetCurrentTimeFromUtcTicks(long ticks)
        {
            var dateTime = new DateTime(ticks, DateTimeKind.Utc);
            return TimeZoneInfo.ConvertTime(dateTime, TimeZoneInfo.Local);
        }

        public static byte[] StreamToByteArray(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static string GetDeviceApiUrl(string serverName, bool useCompanyName)
        {
            var url = Constants.HttpPrefix;

            if (!useCompanyName)
                url += serverName + "/" + Constants.MitServerName + "/api/device/v1/";
            else
                url += Constants.ProxyServer + "/mobileit/proxy/device/" + ConvertToHex(serverName) + "/v1/";

            return url;
        }

        public static string ConvertToHex(string asciiString)
        {
            var hex = "";
            foreach (char c in asciiString)
            {
                int tmp = c;
                hex += String.Format("{0:x2}", Convert.ToUInt32(tmp.ToString())).ToUpper();
            }
            return hex;
        }

        public static String GetBasicAuthorizationHeader(String user, String password)
        {
            return "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(user + ":" + password));
        }

        public static string MemoryStreamToString(MemoryStream stream)
        {
            var array = stream.ToArray();
            return Encoding.UTF8.GetString(array, 0, array.Length);
        }

        public static HttpError GetErrorFromException(Exception error)
        {
            if (error == null)
            {
                return new HttpError();
            }

            if (error is WebException)
            {
                var webException = error as WebException;
                if (webException.Response is HttpWebResponse)
                {
                    return GetErrorFromCustomResponse(webException.Response as HttpWebResponse);
                }
            }

            return new HttpError { Message = error.InnerException != null ? error.InnerException.Message : error.Message };
        }

        public static HttpError GetErrorFromCustomResponse(HttpWebResponse webResponse)
        {
            try
            {
                using (var stream = webResponse.GetResponseStream())
                {
                    var doc = XDocument.Load(stream);
                    if (doc.Root.Name.LocalName == typeof(Error).Name)
                    {
                        var error = new Error();
                        var details = doc.Root.Elements().FirstOrDefault(n => n.Name.LocalName.ToLower() == "details");
                        if (details != null)
                            error.Details = details.Value;

                        var message = doc.Root.Elements().FirstOrDefault(n => n.Name.LocalName.ToLower() == "message");
                        if (message != null)
                            error.Message = message.Value;

                        return new HttpError { Message = error.Message, StatusCode = (int)webResponse.StatusCode };
                    }
                }
            }
            catch
            {
            }

            return GetErrorFromResponse(webResponse);
        }

        public static HttpError GetErrorFromResponse(HttpWebResponse webResponse)
        {
            string message;
            switch (webResponse.StatusCode)
            {
                case HttpStatusCode.Unauthorized:
                    message = "Unauthorized. The requested resource requires authentication.";
                    break;
                case HttpStatusCode.NotFound:
                    message = Constants.ObjectNotFoundMessage;
                    break;
                case HttpStatusCode.ServiceUnavailable:
                    message = "Service unavailable. The server is temporarily unavailable, usually due to high load or maintenance.";
                    break;
                case HttpStatusCode.RequestTimeout:
                    message = "Request timeout.";
                    break;
                case HttpStatusCode.ProxyAuthenticationRequired:
                    message = "The requested proxy requires authentication.";
                    break;
                case HttpStatusCode.Forbidden:
                    message = "Forbidden. The server refuses to fulfill the request.";
                    break;
                default:
                    message = "Unknown error code: " + webResponse.StatusCode;
                    break;
            }

            return new HttpError { Message = message, StatusCode = (int)webResponse.StatusCode };
        }

        public static void ShowError(string message)
        {
            if (String.IsNullOrEmpty(message)) return;
            IoC.Get<IUIManager>().RunOnUI(() => new MessageDialog(message).ShowAsync().AsTask());
        }

        public static string GetStringFromResource(string resourceId)
        {
            return IoC.Get<IUIManager>().GetStringFromResource(resourceId);
        }
    }

    public static class Extensions
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }

        public static bool IsEmpty<T>(this Collection<T> collection)
        {
            return collection.Count == 0;
        }

        public static bool IsEmpty<T>(this IEnumerable<T> collection)
        {
            return !collection.Any();
        }
    }

    public class HistoryItemComparer : IEqualityComparer<HistoryItem>
    {
        public bool Equals(HistoryItem left, HistoryItem right)
        {
            return left.Time == right.Time;
        }
        public int GetHashCode(HistoryItem item)
        {
            return item.Time.GetHashCode();
        }
    }
}
