﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace Quest.Cyclone.Client.Windows.Commons
{
    public class HttpWebRequestSync
    {
        private readonly ManualResetEvent _completedEvent;
        private Exception _exception;

        public HttpWebRequestSync(HttpWebRequest request, string body)
        {
            _completedEvent = new ManualResetEvent(false);

            RawWebRequest = request;
            Body = body;
        }

        public HttpWebRequest RawWebRequest { get; set; }
        public string Body { get; set; }
        public string Response { get; set; }
        public byte[] ByteResponse { get; set; }

        public string SyncPost()
        {
            RawWebRequest.Method = "POST";
            RawWebRequest.BeginGetRequestStream(ar =>
            {
                if (!string.IsNullOrEmpty(Body))
                {
                    try
                    {
                        var requestStream = RawWebRequest.EndGetRequestStream(ar);
                        using (var sw = new StreamWriter(requestStream))
                        {
                            sw.Write(Body);
                        }
                    }
                    catch (Exception e)
                    {
                        _exception = e;
                        _completedEvent.Set();
                    }
                }

                if(_exception == null)
                    RawWebRequest.BeginGetResponse(a =>
                    {
                        try
                        {
                            var responseStream = RawWebRequest.EndGetResponse(a).GetResponseStream();
                            using (var streamReader = new StreamReader(responseStream, Encoding.UTF8))
                            {
                                Response = streamReader.ReadToEnd();
                            }
                        }
                        catch (Exception e)
                        {
                            _exception = e;
                        }
                        finally
                        {
                            _completedEvent.Set();
                        }

                    }, null);

            }, null);

            _completedEvent.WaitOne();

            if (_exception != null)
                throw _exception;

            return Response;
        }

        public string SyncGet()
        {
            RawWebRequest.Method = "GET";
            RawWebRequest.BeginGetResponse(a =>
            {
                try
                {
                    var responseStream = RawWebRequest.EndGetResponse(a).GetResponseStream();
                    using (var streamReader = new StreamReader(responseStream, Encoding.UTF8))
                    {
                        Response = streamReader.ReadToEnd();
                    }
                }
                catch (Exception e)
                {
                    _exception = e;
                }
                finally
                {
                    _completedEvent.Set();
                }

            }, null);

            _completedEvent.WaitOne();

            if (_exception != null)
                throw _exception;

            return Response;
        }

        public byte[] SyncGetBytes()
        {
            RawWebRequest.Method = "GET";
            RawWebRequest.BeginGetResponse(a =>
            {
                try
                {
                    var responseStream = RawWebRequest.EndGetResponse(a).GetResponseStream();
                    ByteResponse = Utils.StreamToByteArray(responseStream);
                }
                catch (Exception e)
                {
                    _exception = e;
                }
                finally
                {
                    _completedEvent.Set();
                }

            }, null);

            _completedEvent.WaitOne();

            if (_exception != null)
                throw _exception;

            return ByteResponse;
        }
    }
}
