﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quest.Cyclone.Client.Windows.Commons.Models;

namespace Quest.Cyclone.Client.Windows.Commons.Commands
{
    public abstract class DatasheetCommandBase
    {
        public abstract void AcceptVisitor(IDatasheetCommandVisitor visitor);
    }
}
