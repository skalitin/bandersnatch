﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quest.Cyclone.Client.Windows.Commons.Models;

namespace Quest.Cyclone.Client.Windows.Commons.Commands
{
    public class ShowDatasheetsCommand : DatasheetCommandBase
    {
        public IEnumerable<DatasheetModel> DatasheetModels { get; set; }
      
        public override void AcceptVisitor(IDatasheetCommandVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
