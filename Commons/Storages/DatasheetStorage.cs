﻿using System;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Models;
using Quest.Cyclone.Client.Windows.Commons.Tasks;

namespace Quest.Cyclone.Client.Windows.Commons.Storages
{
    public class DatasheetStorage : IDatasheetStorage, IHandle<TaskFinishedMessage>
    {
        private readonly Guid _instanceId;
        private readonly IServerRequestsManager _serverRequestsManager;
        private readonly IIconDataRestorer _iconDataRestorer;
        private readonly ISettings _settings;
        private readonly ITaskManager _taskManager;

        public DatasheetStorage(ISettings settings, IServerRequestsManager serverRequestsManager, IIconDataRestorer iconDataRestorer, IEventAggregator eventAggregator, ITaskManager taskManager, Guid instanceId)
        {
            _settings = settings;
            _serverRequestsManager = serverRequestsManager;
            _iconDataRestorer = iconDataRestorer;
            _taskManager = taskManager;
            _instanceId = instanceId;

            eventAggregator.Subscribe(this);
            RootDatasheetModel = new DatasheetModel()
            {
                Id = Guid.Empty,
                IsExpandable = true,
                HasRecords = false
            };
        }

        public DatasheetModel RootDatasheetModel { get; set; }

        public void LoadDatasheets(Guid datasheetId)
        {
            var task = new GetDatasheetsTask(datasheetId, _instanceId, true);
            _taskManager.RunTask(task);
        }

        public void Handle(TaskFinishedMessage message)
        {
            var task = message.Task as DatasheetsTask;
            if (task != null)
            {
                if (task is GetDatasheetsTask)
                {
                    var datasheetModel = RootDatasheetModel.GetDatasheetModel(task.DatasheetId);
                    datasheetModel.SetDatasheets(task.Response.Datasheets);
                }
            }
        }
    }
}
