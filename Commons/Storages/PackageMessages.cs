﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;

namespace Quest.Cyclone.Client.Windows.Commons.Storages
{
    public class PackageMessage
    {
    }

    public class PackageCountChangedMessage : PackageMessage
    {
    }

    public class PackageBadgeChangedMessage : PackageMessage
    {
        public PackageBadgeChangedMessage(Guid packageId)
        {
            PackageId = packageId;
        }

        public Guid PackageId { get; set; }
    }
}
