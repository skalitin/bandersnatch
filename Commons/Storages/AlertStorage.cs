﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Storages
{
    public class AlertStorage : IAlertStorage, IHandle<SettingMessage>
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ISettings _settings;
        private readonly ConcurrentDictionary<Guid, AlertContainer> _alertContainerByPackage;

        public AlertStorage(ISettings settings, IEventAggregator eventAggregator)
        {
            _settings = settings;
            _eventAggregator = eventAggregator;
            _alertContainerByPackage = new ConcurrentDictionary<Guid, AlertContainer>();
        }

        public void RemoveAlerts()
        {
            var packageIds = (from Guid packageId in _alertContainerByPackage.Keys
                             where (GetAlerts(packageId, AlertStatus.New).Count > 0)
                             select packageId).ToArray();

            _alertContainerByPackage.Clear();

            foreach (var packageId in packageIds)
            {
                var message = new NewAlertsChangedMessage(packageId);
                _eventAggregator.Publish(message);                                
            }
        }

        public void RemoveAlerts(Guid packageId, IEnumerable<AlertId> alertIds)
        {
            MonitorNewAlertsDuring(packageId, () => GetAlertContainer(packageId).Remove(alertIds.Select(alert => alert.Id)));
        }

        public void RemoveAlerts(Guid packageId, AlertStatus status)
        {
            MonitorNewAlertsDuring(packageId, () => GetAlertContainer(packageId).Remove(status));
        }

        public void SetAlerts(Guid packageId, IEnumerable<Alert> alerts)
        {
            MonitorNewAlertsDuring(packageId, () =>
            {
                var alertContainer = _alertContainerByPackage.AddOrUpdate(packageId, new AlertContainer(), (key, oldValue) => oldValue);
                alertContainer.Set(alerts);
            });
        }

        public BindableCollection<Alert> GetAlerts(Guid packageId)
        {
            return GetAlertContainer(packageId).Alerts;
        }

        public BindableCollection<Alert> GetAlerts(Guid packageId, AlertStatus status)
        {
            return GetAlertContainer(packageId).GetCollection(status);
        }

        public ICollection<Alert> GetAlerts(AlertStatus status)
        {
            var alerts = new List<Alert>();
            foreach (var container in _alertContainerByPackage.Values)
            {
                alerts.AddRange(container.GetCollection(status));
            }

            return alerts;
        }

        public bool HasAlerts(Guid packageId, AlertStatus status)
        {
            return GetAlerts(packageId, status).Count > 0;
        }

        public void UpdateAlerts(Guid packageId, IEnumerable<Alert> alerts)
        {
            foreach (var alert in alerts)
            {
                UpdateAlert(packageId, alert);
            }
        }

        public void UpdateAlert(Guid packageId, Alert alert)
        {
            MonitorNewAlertsDuring(packageId, () => 
            {
                var container = GetAlertContainer(packageId);
                var existingAlert = container.Alerts.FirstOrDefault(each => each.Id == alert.Id);
                if (existingAlert == null)
                {
                    container.Add(alert);
                }
                else
                {
                    UpdateAlert(packageId, existingAlert, alert);
                }
            });
        }

        private void UpdateAlert(Guid packageId, Alert existingAlert, Alert alert)
        {
            var container = GetAlertContainer(packageId);
            if (existingAlert.Brief.Status != alert.Brief.Status)
            {
                container.Remove(existingAlert);
            }

            existingAlert.UpdateFrom(alert);
            container.Add(existingAlert);
        }

        private AlertContainer GetAlertContainer(Guid packageId)
        {
            return _alertContainerByPackage.GetOrAdd(packageId, new AlertContainer());
        }

        public void Handle(SettingMessage settingMessage)
        {
            if ((settingMessage.Is(() => _settings.IsLoggedOn)) && !_settings.IsLoggedOn)
            {
                RemoveAlerts();
            }
        }

        private void MonitorNewAlertsDuring(Guid packageId, System.Action action)
        {
            var originalNewAlertsCount = GetAlerts(packageId, AlertStatus.New).Count;
            action.Invoke();

            var newAlertsCount = GetAlerts(packageId, AlertStatus.New).Count;
            if (originalNewAlertsCount != newAlertsCount)
            {
                var message = new NewAlertsChangedMessage(packageId);
                _eventAggregator.Publish(message);                
            }
        }
    }
}
