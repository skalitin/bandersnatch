﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Storages
{
    public class PackageStorage : IPackageStorage, IHandle<SettingMessage>
    {
        private readonly ISettings _settings;
        private readonly IEventAggregator _eventAggregator;
        private readonly BindableCollection<Package> _packages;

        public PackageStorage(ISettings settings, IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _settings = settings;
            _packages = new BindableCollection<Package>();
        }

        public void AddPackage(Package package)
        {
            AddPackages(new Collection<Package>{package});
        }

        public void AddPackages(ICollection<Package> packages)
        {
            _packages.AddRange(packages);
            _eventAggregator.Publish(new PackageCountChangedMessage());
        }

        public void SetPackages(ICollection<Package> packages)
        {
            _packages.Clear();
            _packages.AddRange(packages);
            _eventAggregator.Publish(new PackageCountChangedMessage());
        }

        public void RemovePackages(ICollection<Guid> ids)
        {
            var packagesToRemove = _packages.Where(package => ids.Contains(package.Id)).ToList();
            foreach (var packageToRemove in packagesToRemove)
            {
                _packages.Remove(packageToRemove);
            }
            _eventAggregator.Publish(new PackageCountChangedMessage());
        }

        public void RemovePackages()
        {
            _packages.Clear();
            _eventAggregator.Publish(new PackageCountChangedMessage());
        }

        public void RemovePackage(Guid id)
        {
            RemovePackages(new []{id});
        }

        public void UpdatePackageBadgeCount(Guid packageId, int badge)
        {
            var package = Packages.FirstOrDefault(each => each.Id == packageId);
            if (package == null) return;

            if (package.Badge != badge)
            {
                _eventAggregator.Publish(new PackageBadgeChangedMessage(packageId));
                package.Badge = badge;
            }
        }

        public BindableCollection<Package> Packages
        {
            get
            {
                return _packages;
            }
        }

        public Package GetPackage(Guid packageId)
        {
            return _packages.FirstOrDefault(each => each.Id == packageId);
        }

        public void Handle(SettingMessage settingMessage)
        {
            if (settingMessage.Is(() => _settings.DeviceStatus))
            {
                if (_settings.DeviceStatus == DeviceStatusType.NotApproved ||
                    _settings.DeviceStatus == DeviceStatusType.Blocked)
                {
                    RemovePackages();
                }
            }
            else if (settingMessage.Is(() => _settings.IsLoggedOn))
            {
                if (!_settings.IsLoggedOn)
                {
                    RemovePackages();
                }
            }
        }
    }
}
