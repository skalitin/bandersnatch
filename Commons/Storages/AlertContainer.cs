﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;

namespace Quest.Cyclone.Client.Windows.Commons.Storages
{
    class AlertContainer
    {
        private readonly object _syncRoot = new Object();
        private readonly ConcurrentDictionary<AlertStatus, BindableCollection<Alert>> _alertsByStatus;

        public AlertContainer()
        {
            _alertsByStatus = new ConcurrentDictionary<AlertStatus, BindableCollection<Alert>>();
            _alertsByStatus[AlertStatus.New] = new BindableCollection<Alert>();
            _alertsByStatus[AlertStatus.Acknowledged] = new BindableCollection<Alert>();
            _alertsByStatus[AlertStatus.Resolved] = new BindableCollection<Alert>();
        }

        public BindableCollection<Alert> Alerts
        {
            get
            {
                lock (_syncRoot)
                {
                    var alerts = new BindableCollection<Alert>();
                    foreach (var collection in _alertsByStatus.Values)
                    {
                        alerts.AddRange(collection);
                    }

                    return alerts;
                }
            }
        }

        public void ClearAll()
        {
            lock (_syncRoot)
            {
                foreach (var collection in _alertsByStatus.Values)
                {
                    collection.Clear();
                }
            }
        }

        public void Set(IEnumerable<Alert> alerts)
        {
            lock (_syncRoot)
            {
                var groups = alerts.GroupBy(each => each.Brief.Status);
                foreach (var grouping in groups)
                {
                    var status = grouping.Key;
                    var sorted = grouping.OrderByDescending(alert => alert.Brief.Time);
                    var collection = GetCollection(status);

                    collection.Clear();
                    collection.AddRange(sorted);
                }
            }
        }

        public void Add(Alert alert)
        {
            lock (_syncRoot)
            {
                if (Alerts.Any(a => a.Id == alert.Id))
                {
                    return;
                }

                var colection = GetCollection(alert.Brief.Status);
                for (var index = colection.Count - 1; index >= 0; --index)
                {
                    if (colection.ElementAt(index).Brief.Time > alert.Brief.Time)
                    {
                        colection.Insert(index, alert);
                        return;
                    }
                }
                colection.Insert(0, alert);
            }
        }

        public void Remove(IEnumerable<Guid> ids)
        {
            var list = ids.ToList();
            if (list.IsEmpty())
            {
                return;
            }

            Remove(alert => list.Contains(alert.Id));
        }

        public void Remove(AlertStatus status)
        {
            Remove(alert => alert.Brief.Status == status);
        }

        public void Remove(Func<Alert, bool> predicate)
        {
            lock (_syncRoot)
            {
                foreach (var collection in _alertsByStatus.Values)
                {
                    var alerts = collection.Where(predicate).ToList();

                    // Do not remove empty ranges due to unnecessary visual transitions in associated controls
                    if (alerts.Count > 0)
                    {
                        collection.RemoveRange(alerts);
                    }
                }
            }
        }

        public void Remove(Alert alert)
        {
            lock (_syncRoot)
            {
                foreach (var collection in _alertsByStatus.Values)
                {
                    collection.Remove(alert);
                }
            }
        }

        public BindableCollection<Alert> GetCollection(AlertStatus status)
        {
            return _alertsByStatus[status];
        }
    }
}
