﻿using System;
using System.Linq;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.Commands;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Models;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public class GetDatasheetsTask : DatasheetsTask
    {
        private readonly bool _refresh;

        public GetDatasheetsTask(ISettings settings, IServerRequestsManager serverRequestsManager, IIconDataRestorer iconDataRestorer, Guid datasheetId, Guid instanceId, bool refresh)
            : base(settings, serverRequestsManager, iconDataRestorer, datasheetId, instanceId)
        {
            _refresh = refresh;
        }

        public GetDatasheetsTask(Guid datasheetId, Guid instanceId, bool refresh)
            : this(IoC.Get<ISettings>(), IoC.Get<IServerRequestsManager>(), IoC.Get<IIconDataRestorer>(), datasheetId, instanceId, refresh)
        {
        }

        protected override void ExecuteInternal()
        {
            _response = _serverRequestsManager.GetDatasheets(_instanceId, _datasheetId, _refresh);
            if (_response.ResponseType != DatasheetResponseType.Datasheets)
            {
                throw new Exception("Unexpected response type");
            }

            var datasheetModels = from Datasheet datasheet in _response.Datasheets select new DatasheetModel(datasheet);
            foreach (var datasheetModel in datasheetModels)
            {
                RestoreIcon(datasheetModel);
            }

            Command = new ShowDatasheetsCommand
            {
                DatasheetModels = datasheetModels
            };
        }

        private async void RestoreIcon(DatasheetModel datasheetModel)
        {
            var icon = await _iconDataRestorer.RestoreIconDataAsync(datasheetModel.Icon);
            datasheetModel.Icon = icon;
        }
    }
}
