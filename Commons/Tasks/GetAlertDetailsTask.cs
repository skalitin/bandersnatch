﻿using System;
using System.Collections.Generic;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public class GetAlertDetailsTask : TaskBase
    {
        private readonly IAlertStorage _alertStorage;
        private readonly IServerRequestsManager _serverRequestsManager;
        private readonly Guid _packageId;
        private readonly Guid _alertId;

        public GetAlertDetailsTask(IAlertStorage alertStorage, ISettings settings, IServerRequestsManager serverRequestsManager, Guid alertId, Guid packageId)
            : base(settings)
        {
            _alertStorage = alertStorage;
            _serverRequestsManager = serverRequestsManager;
            _packageId = packageId;
            _alertId = alertId;
        }

        protected override void ExecuteInternal()
        {
            var alert = _serverRequestsManager.GetAlertDetails(_alertId);
            if (alert != null)
            {
                _alertStorage.UpdateAlert(_packageId, alert);
            }
        }

        public bool Uses(Alert alert)
        {
            return alert != null && alert.Id == _alertId;
        }
    }
}
