﻿using System;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public class ResolveAlertTask : TaskBase
    {
        private readonly IServerRequestsManager _serverRequestsManager;
        private readonly IAlertStorage _alertStorage;
        private readonly Guid _alertId;
        private readonly string _comment;
        private readonly Guid _packageId;

        public ResolveAlertTask(ISettings settings, IServerRequestsManager serverRequestsManager, IAlertStorage alertStorage, Guid packageId, Guid alertId, string comment = "")
            : base(settings)
        {
            _serverRequestsManager = serverRequestsManager;
            _alertStorage = alertStorage;
            _alertId = alertId;
            _comment = comment;
            _packageId = packageId;
        }

        protected override void ExecuteInternal()
        {
            _serverRequestsManager.ResolveAlert(_alertId, _comment);

            var alert = _serverRequestsManager.GetAlertDetails(_alertId);
            _alertStorage.UpdateAlert(_packageId, alert);
        }

        public bool Uses(Alert alert)
        {
            return alert != null && alert.Id == _alertId;
        }
    }
}
