﻿using System;
using System.Net;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public class RegisterTask : TaskBase
    {
        private readonly IServerRequestsManager _serverRequestsManager;

        public RegisterTask(ISettings settings, IServerRequestsManager serverRequestsManager)
            :base(settings)
        {
            _serverRequestsManager = serverRequestsManager;
        }

        protected override void ExecuteInternal()
        {
            var response = _serverRequestsManager.RegisterDevice();

            var version = "";
            try
            {
                var metadata = _serverRequestsManager.GetServerMetadata();
                var serverVersion = metadata.ServerVersion;
                version = serverVersion.FileVersion;
            }
            catch (Exception serverMetadataError)
            {
                if (Utils.GetErrorFromException(serverMetadataError).StatusCode == (int)HttpStatusCode.NotFound)
                {
                    try
                    {
                        var serverVersion = _serverRequestsManager.GetServerVersion();
                        version = serverVersion.FileVersion;
                    }
                    catch (Exception serverVersionError)
                    {
                        if (Utils.GetErrorFromException(serverVersionError).StatusCode != (int)HttpStatusCode.NotFound)
                        {
                            throw;
                        }
                    }
                }
                else
                {
                    throw;
                }
            }


            _settings.ServerVersion = version;
            _settings.UserToken = response.Password;
        }
    }
}
