﻿using System;
using System.Collections.Generic;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public class GetPackagesTask : TaskBase
    {
        private readonly IServerRequestsManager _serverRequestsManager;
        private readonly IPackageStorage _packageStorage;
        private readonly IIconDataRestorer _iconDataRestorer;

        public GetPackagesTask(ISettings settings, IServerRequestsManager serverRequestsManager, IPackageStorage packageStorage, IIconDataRestorer iconDataRestorer)
            : base(settings)
        {
            _serverRequestsManager = serverRequestsManager;
            _packageStorage = packageStorage;
            _iconDataRestorer = iconDataRestorer;
        }

        public override IEnumerable<Type> TasksToWait()
        {
            return new List<Type>() { typeof(GetPackagesTask) };
        }

        public override IEnumerable<Type> TasksToCancel()
        {
            return new List<Type>() { typeof(GetPackagesTask) };
        }

        protected override void ExecuteInternal()
        {
            var packagesResponse = _serverRequestsManager.GetPackages();
            if (IsCanceled) return;

            foreach (var package in packagesResponse.Packages)
            {
                if (IsCanceled) return;
                package.Icon = _iconDataRestorer.RestoreIconData(package.Icon);
            }
            _packageStorage.SetPackages(packagesResponse.Packages);
            _settings.SyncId = packagesResponse.SyncId;
        }
    }
}
