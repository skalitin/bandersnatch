﻿using System;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public class GetAlertsTask : TaskBase
    {
        private readonly IAlertStorage _alertStorage;
        private readonly Guid _packageId;
        private readonly AlertStatus _status;
        private readonly IServerRequestsManager _serverRequestsManager;

        public GetAlertsTask(IAlertStorage alertStorage, ISettings settings, IServerRequestsManager serverRequestsManager, Guid packageId, AlertStatus status)
            : base(settings)
        {
            _alertStorage = alertStorage;
            _serverRequestsManager = serverRequestsManager;
            _packageId = packageId;
            _status = status;
        }

        protected override void ExecuteInternal()
        {
            if (IsCanceled) return;
            var alerts = _serverRequestsManager.GetAlerts(_packageId, _status, 100/*TODO*/, _settings.SyncId);

            if (IsCanceled) return;
            _alertStorage.SetAlerts(_packageId, alerts.Items);
        }

        public bool Uses(AlertStatus status)
        {
            return _status == status;
        }
    }
}
