﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public class TaskManager : ITaskManager
    {
        class TaskWrapper
        {
            private AutoResetEvent _waitEvent;
            private readonly Object _waitEventSyncRoot = new Object();

            private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
            private readonly TimeSpan _repeatInterval;
            private readonly IEventAggregator _eventAggregator;

            public TaskWrapper(IEventAggregator eventAggregator, TaskBase task)
            {
                _eventAggregator = eventAggregator;
                Task = task;
            }

            public TaskWrapper(IEventAggregator eventAggregator, TaskBase task, Guid packageId)
            {
                _eventAggregator = eventAggregator;
                PackageId = packageId;
                Task = task;
            }

            public TaskWrapper(IEventAggregator eventAggregator, TaskBase task, TimeSpan repeatInterval)
            {
                _eventAggregator = eventAggregator;
                Task = task;
                _repeatInterval = repeatInterval;
            }

            public TaskWrapper(IEventAggregator eventAggregator, TaskBase task, Guid packageId, TimeSpan repeatInterval)
            {
                _eventAggregator = eventAggregator;
                PackageId = packageId;
                Task = task;
                _repeatInterval = repeatInterval;
            }

            public void Wait()
            {
                lock (_waitEventSyncRoot)
                {
                    if (_waitEvent != null)
                    {
                        _waitEvent.WaitOne();
                        _waitEvent.Set();
                    }
                }
            }

            public void Cancel()
            {
                _cancellationTokenSource.Cancel();
                Task.Cancel();    
            }

            public void Run()
            {
                if (_repeatInterval.TotalMilliseconds > 0)
                {
                    RunPeriodic();
                }
                else
                {
                    RunOnce();
                }
            }

            private void RunPeriodic()
            {
                RunOnce();

                var cancellationToken = _cancellationTokenSource.Token;
                System.Action body = () =>
                {
                    while (true)
                    {
                        if (cancellationToken.WaitHandle.WaitOne(_repeatInterval))
                        {
                            break;
                        }

                        RunOnce();
                    }
                };

                var task = System.Threading.Tasks.Task.Factory.StartNew(body, cancellationToken, TaskCreationOptions.LongRunning, TaskScheduler.Default);
                task.Wait();
            }

            private void RunOnce()
            {
                try
                {
                    lock (_waitEventSyncRoot)
                    {
                        _waitEvent = new AutoResetEvent(false);
                    }

                    IsRunning = true;
                    System.Threading.Tasks.Task.Run(() => _eventAggregator.Publish(new TaskStartedMessage { Task = Task }));
                    Task.Execute();
                }
                finally
                {
                    IsRunning = false;
                    lock (_waitEventSyncRoot)
                    {
                        _waitEvent.Set();
                        _waitEvent = null;
                    }

                    System.Threading.Tasks.Task.Run(() => _eventAggregator.Publish(new TaskFinishedMessage { Task = Task }));
                }
            }

            public bool IsRunning { get; private set; }
            public Guid PackageId { get; private set; }
            public TaskBase Task { get; private set; }

            public override string ToString()
            {
                return String.Format("{0}({1})", GetType().Name, Task);
            }
        }

        private readonly IEventAggregator _eventAggregator;
        private readonly object _syncRoot = new Object();
        private readonly List<TaskWrapper> _taskWrappers = new List<TaskWrapper>();

        public TaskManager(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }
        
        //_dependencies.Add(typeof(GetPackagesTask), new List<Type> { typeof(GetPackagesTask) });
        //_dependencies.Add(typeof(GetUpdatesTask), new List<Type> { typeof(GetUpdatesTask), typeof(GetPackagesTask) });
        //_dependencies.Add(typeof(GetReportContentTask), new List<Type> {typeof (CancelDatasheetParametersTask)});
        //_dependencies.Add(typeof(GetReportsTask), new List<Type> {typeof(CancelDatasheetParametersTask)});
        //_dependencies.Add(typeof(RunDatasheetActionTask), new List<Type> {typeof (SetDatasheetParametersTask)});
        //_dependencies.Add(typeof(GetReportActionsTask), new List<Type> {typeof (SetDatasheetParametersTask)});
            
        //_concurrences.Add(typeof(GetNewAlertsTask), new List<Type> { typeof(GetNewAlertsTask) });
        //_concurrences.Add(typeof(GetAckAlertsTask), new List<Type> { typeof(GetAckAlertsTask) });
        //_concurrences.Add(typeof(GetResolvedAlertsTask), new List<Type> { typeof(GetResolvedAlertsTask) });
        //_concurrences.Add(typeof(GetUpdatesTask), new List<Type> { typeof(GetUpdatesTask) });
        //_concurrences.Add(typeof(UnregisterTask), new List<Type> { typeof(GetPackagesTask) });
        //_concurrences.Add(typeof(GetPackagesTask), new List<Type> { typeof(GetPackagesTask) });
        //_concurrences.Add(typeof(GetReportContentTask), new List<Type> { typeof(GetReportContentTask), typeof(RunDatasheetActionTask), typeof(SetDatasheetParametersTask) });
        //_concurrences.Add(typeof(GetReportsTask), new List<Type> { typeof(GetReportsTask), typeof(SetDatasheetParametersTask) });
        //_concurrences.Add(typeof(RunDatasheetActionTask), new List<Type> { typeof(RunDatasheetActionTask) });
        //_concurrences.Add(typeof(GetReportActionsTask), new List<Type> { typeof(GetReportActionsTask) });


        public void RunPeriodicTask<T>(Guid packageId, TimeSpan interval) where T : TaskBase
        {
            RunPeriodicTask(packageId, IoC.Get<T>(), interval);
        }

        public void RunTask(TaskBase task)
        {
            Run(new TaskWrapper(_eventAggregator, task));
        }

        public void RunTask<T>() where T : TaskBase
        {
            RunTask<T>(Guid.Empty);
        }

        public void RunTask(Guid packageId, TaskBase task)
        {
            Run(new TaskWrapper(_eventAggregator, task, packageId));
        }

        public void RunTask<T>(Guid packageId) where T : TaskBase
        {
            Run(new TaskWrapper(_eventAggregator, IoC.Get<T>(), packageId));
        }

        public void RunPeriodicTask(TaskBase task, TimeSpan interval)
        {
            Run(new TaskWrapper(_eventAggregator, task, interval));
        }

        public void RunPeriodicTask<T>(TimeSpan interval) where T : TaskBase
        {
            RunPeriodicTask(IoC.Get<T>(), interval);
        }

        public void RunPeriodicTask(Guid packageId, TaskBase task, TimeSpan interval)
        {
            Run(new TaskWrapper(_eventAggregator, task, packageId, interval));
        }

        private void Run(TaskWrapper taskWrapper)
        {
            var task = taskWrapper.Task;
            var packageId = taskWrapper.PackageId;

            IEnumerable<TaskWrapper> wrappersToWait;
            lock (_syncRoot)
            {
                var taskTypesToCancel = task.TasksToCancel();
                var wrappersToCancel = _taskWrappers.Where(
                    each => each.PackageId == packageId && 
                    taskTypesToCancel.Contains(each.Task.GetType())).ToList();

                foreach (var wrapper in wrappersToCancel)
                {
                    wrapper.Cancel();
                }

                var taskTypesToWait = task.TasksToWait();
                wrappersToWait = _taskWrappers.Where(
                        each => each.PackageId == packageId && 
                        taskTypesToWait.Contains(each.Task.GetType()) &&
                        each.IsRunning).ToList();

                _taskWrappers.Add(taskWrapper);
            }

            Task.Run(() =>
            {
                try
                {
                    foreach (var wrapper in wrappersToWait)
                    {
                        wrapper.Wait();
                    }
                    taskWrapper.Run();
                }
                finally
                {
                    lock (_syncRoot)
                    {
                        _taskWrappers.Remove(taskWrapper);
                    }
                }
            });
        }

        public bool IsTaskRunning<T>() where T : TaskBase
        {
            return IsTaskRunning<T>(task => true);
        }

        public bool IsTaskRunning<T>(Func<T, bool> predicate) where T : TaskBase
        {
            lock (_syncRoot)
            {
                return _taskWrappers.Any(wrapper =>
                {
                    var task = wrapper.Task;
                    return (task is T && wrapper.IsRunning && predicate(task as T));
                });
            }
        }

        public void CancelTask<T>()
        {
            lock (_syncRoot)
            {
                var wrappers = _taskWrappers.Where(wrapper => wrapper.Task.GetType() == typeof(T));
                foreach (var taskWrapper in wrappers)
                {
                    taskWrapper.Cancel();
                }
            }
        }
    }
}
