﻿using System;
using System.Collections.Generic;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public class UnregisterTask : TaskBase
    {
        private readonly ITaskManager _taskManager;
        private readonly IServerRequestsManager _serverRequestsManager;

        public UnregisterTask(ISettings settings, IServerRequestsManager serverRequestsManager, ITaskManager taskManager)
            :base(settings)
        {
            _serverRequestsManager = serverRequestsManager;
            _taskManager = taskManager;
        }

        public override IEnumerable<Type> TasksToCancel()
        {
            return new List<Type>() { typeof(GetUpdatesTask), typeof(GetPackagesTask) };
        }

        protected override void ExecuteInternal()
        {
            var username = _settings.UserName;
            var usertoken = _settings.UserToken;

            _taskManager.CancelTask<GetUpdatesTask>();

            _settings.UserName = string.Empty;
            _settings.UserPassword = string.Empty;
            _settings.UserToken = string.Empty;

            try
            {
                _serverRequestsManager.UnsubscribeDevice(username, usertoken);
            }
            catch (Exception)
            {
                // Ignore all errors on unsubscribe
            }
        }
    }
}
