﻿using System;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public class AcknowledgeAlertTask : TaskBase
    {
        private readonly IServerRequestsManager _serverRequestsManager;
        private readonly IAlertStorage _alertStorage;
        private readonly Guid _alertId;
        private readonly Guid _packageId;
        
        public AcknowledgeAlertTask(ISettings settings, IServerRequestsManager serverRequestsManager, IAlertStorage alertStorage, Guid packageId, Guid alertId)
            : base(settings)
        {
            _serverRequestsManager = serverRequestsManager;
            _alertStorage = alertStorage;
            _alertId = alertId;
            _packageId = packageId;
        }

        protected override void ExecuteInternal()
        {
            _serverRequestsManager.AcknowledgeAlert(_alertId);

            var alert = _serverRequestsManager.GetAlertDetails(_alertId);
            _alertStorage.UpdateAlert(_packageId, alert);
        }

        public bool Uses(Alert alert)
        {
            return alert != null && alert.Id == _alertId;
        }
    }
}
