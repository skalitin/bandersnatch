namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public abstract class TaskMessage
    {
        public TaskBase Task { get; set; }
    }

    public class TaskStartedMessage : TaskMessage
    {
    }

    public class TaskFinishedMessage : TaskMessage
    {
    }
}