﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public class GetUpdatesTask : TaskBase
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly IServerRequestsManager _serverRequestsManager;
        private readonly IPackageStorage _packageStorage;
        private readonly IAlertStorage _alertStorage;
        private readonly IIconDataRestorer _iconDataRestorer;
        
        public GetUpdatesTask(ISettings settings, IEventAggregator eventAggregator,  IServerRequestsManager serverRequestsManager, IPackageStorage packageStorage, IAlertStorage alertStorage, IIconDataRestorer iconDataRestorer)
            :base(settings)
        {
            _eventAggregator = eventAggregator;
            _serverRequestsManager = serverRequestsManager;
            _packageStorage = packageStorage;
            _alertStorage = alertStorage;
            _iconDataRestorer = iconDataRestorer;
        }

        public override IEnumerable<Type> TasksToWait()
        {
            return new List<Type>() { typeof(GetUpdatesTask), typeof(GetPackagesTask) };
        }

        public override IEnumerable<Type> TasksToCancel()
        {
            return new List<Type>() { typeof(GetUpdatesTask) };
        }

        protected override void ExecuteInternal()
        {
            if (IsCanceled || !_settings.IsLoggedOn)
            {
                return;
            }

            var updatesResponse = _serverRequestsManager.GetUpdates(_settings.SyncId);
            ProceedPackagesUpdates(updatesResponse.Packages.ToList());
            _settings.SyncId = updatesResponse.SyncId;
        }

        public void ProceedPackagesUpdates(IEnumerable<PackageUpdates> updates)
        {
            ProceedAddedPackages(updates);
            ProceedRemovedPackages(updates);
            ProceedCommonPackagesUpdates(updates);
        }

        private void ProceedCommonPackagesUpdates(IEnumerable<PackageUpdates> updates)
        {
            foreach (var packageUpdates in updates)
            {
                _packageStorage.UpdatePackageBadgeCount(packageUpdates.Id, packageUpdates.Badge);
                _alertStorage.UpdateAlerts(packageUpdates.Id, packageUpdates.Alerts);

                ProceedAlertsRemovedPackageUpdates(packageUpdates);
                //_packageStorage.UpdatePackageInstances(packageUpdates);
            }
        }

        private void ProceedAlertsRemovedPackageUpdates(PackageUpdates packageUpdates)
        {
            var removedIds = packageUpdates.Removed.Select(alert => alert.Id);
            var alertsToRemove = _alertStorage.GetAlerts(packageUpdates.Id).Where(alert => removedIds.Contains(alert.Id));
            _alertStorage.RemoveAlerts(packageUpdates.Id, packageUpdates.Removed);
            foreach (var alert in alertsToRemove)
            {
                var updatedAlert = _serverRequestsManager.GetAlertDetails(alert.Id);
                if (updatedAlert == null)
                {
                    continue;
                }

                // Updating the 'Owner' field in existing alert to show it in notification
                alert.UpdateFrom(updatedAlert);
                alert.IsRemoved = true;

                _eventAggregator.Publish(new AlertRemovedMessage(alert.Id));
            }
        }

        private void ProceedAddedPackages(IEnumerable<PackageUpdates> updates)
        {
            var packages = updates.Where(update => _packageStorage.Packages.All(package => package.Id != update.Id)).ToList();

            foreach (var package in packages)
            {
                var packageToAdd = _serverRequestsManager.GetPackage(package.Id);
                packageToAdd.Icon = _iconDataRestorer.RestoreIconData(packageToAdd.Icon);
                _packageStorage.AddPackage(packageToAdd);
            }
        }

        private void ProceedRemovedPackages(IEnumerable<PackageUpdates> updates)
        {
            var packagesToRemove = _packageStorage.Packages.Where(package => updates.All(update => update.Id != package.Id)).ToList();
            var packagesIdsToRemove = packagesToRemove.Select(package => package.Id).ToList();
            _packageStorage.RemovePackages(packagesIdsToRemove);
        }
    }
}
