﻿using System;
using System.Collections.Generic;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public class GetNewAlertsTask : GetAlertsTask
    {
        public GetNewAlertsTask(IAlertStorage alertStorage, ISettings settings, IServerRequestsManager serverRequestsManager, Guid packageId)
            : base(alertStorage, settings, serverRequestsManager, packageId, AlertStatus.New)
        {
        }

        public override IEnumerable<Type> TasksToCancel()
        {
            return new List<Type>() { typeof(GetNewAlertsTask) };
        }
    }
}
