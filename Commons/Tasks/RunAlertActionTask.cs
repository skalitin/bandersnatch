﻿using System;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public class RunAlertActionTask : TaskBase
    {
        private readonly IServerRequestsManager _serverRequestsManager;
        private readonly IAlertStorage _alertStorage;
        private readonly Guid _packageId;
        private readonly Guid _alertId;
        private readonly Guid _actionId;

        public RunAlertActionTask(ISettings settings, IServerRequestsManager serverRequestsManager, IAlertStorage alertStorage, Guid packageId, Guid alertId, Guid actionId)
            : base(settings)
        {
            _serverRequestsManager = serverRequestsManager;
            _alertStorage = alertStorage;
            _packageId = packageId;
            _alertId = alertId;
            _actionId = actionId;
        }

        protected override void ExecuteInternal()
        {
            _serverRequestsManager.RunAlertAction(_actionId);

            var alert = _serverRequestsManager.GetAlertDetails(_alertId);
            _alertStorage.UpdateAlert(_packageId, alert);
        }

        public bool Uses(Alert alert)
        {
            return alert != null && alert.Id == _alertId;
        }
    }
}
