﻿using System;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.Commands;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Quest.Cyclone.Client.Windows.Commons.Models;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public class GetDatasheetContentTask : DatasheetsTask
    {
        private readonly Range _range;
        private readonly bool _refresh;

        public GetDatasheetContentTask(ISettings settings, IServerRequestsManager serverRequestsManager, IIconDataRestorer iconDataRestorer, Guid datasheetId, Guid instanceId, Range range, bool refresh)
            : base(settings, serverRequestsManager, iconDataRestorer, datasheetId, instanceId)
        {
            _range = range;
            _refresh = refresh;
        }

        public GetDatasheetContentTask(Guid datasheetId, Guid instanceId, Range range, bool refresh)
            : this(IoC.Get<ISettings>(), IoC.Get<IServerRequestsManager>(), IoC.Get<IIconDataRestorer>(), datasheetId, instanceId,  range, refresh)
        {
        }

        protected override void ExecuteInternal()
        {
            _response = _serverRequestsManager.GetDatasheetContent(_instanceId, _datasheetId, _range, _refresh);
            if (_response.ResponseType != DatasheetResponseType.DatasheetContent)
            {
                throw new Exception("Unexpected response type");
            }

            var datasheetContentModel = new DatasheetContentModel(_response.Content);
            foreach (var record in datasheetContentModel.Records)
            {
                RestoreIcon(record);
            }

            Command = new ShowDatasheetContentCommand
            {
                DatasheetContentModel = datasheetContentModel
            };
        }

        private async void RestoreIcon(DatasheetRecord datasheetRecord)
        {
            var icon = await _iconDataRestorer.RestoreIconDataAsync(datasheetRecord.Icon);
            datasheetRecord.Icon = icon;
        }
    }
}
