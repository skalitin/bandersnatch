﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.Commands;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public abstract class DatasheetsTask : TaskBase
    {
        protected readonly IServerRequestsManager _serverRequestsManager;
        protected readonly IIconDataRestorer _iconDataRestorer;
        protected readonly Guid _datasheetId;
        protected readonly Guid _instanceId;
        protected DatasheetResponse _response;

        protected DatasheetsTask(ISettings settings, IServerRequestsManager serverRequestsManager, IIconDataRestorer iconDataRestorer, Guid datasheetId, Guid instanceId)
            :base(settings)
        {
            _serverRequestsManager = serverRequestsManager;
            _iconDataRestorer = iconDataRestorer;
            _datasheetId = datasheetId;
            _instanceId = instanceId;
        }

        public DatasheetResponse Response
        {
            get { return _response; }
        }

        public Guid DatasheetId
        {
            get { return _datasheetId; }
        }

        public DatasheetCommandBase Command { get; set; }
    }
}
