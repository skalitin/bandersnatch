﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons.Tasks
{
    public abstract class TaskBase
    {
        protected readonly ISettings _settings;

        protected TaskBase(ISettings settings)
        {
            _settings = settings;
        }

        public bool IsCanceled { get; set; }

        protected abstract void ExecuteInternal();
        
        protected virtual void CancelInternal()
        {
        }

        public virtual IEnumerable<Type> TasksToWait()
        {
            return new List<Type>();
        }

        public virtual IEnumerable<Type> TasksToCancel()
        {
            return new List<Type>();
        }

        public void Execute()
        {
            try
            {
                if (!IsCanceled)
                {
                    ExecuteInternal();
                    _settings.DeviceStatus = new DeviceStatus(DeviceStatusType.Approved);
                }
            }
            catch (Exception exception)
            {
                var error = Utils.GetErrorFromException(exception);

                switch (error.StatusCode)
                {
                    case Constants.DeviceNotApprovedStatusCode:
                        _settings.DeviceStatus = new DeviceStatus(
                            DeviceStatusType.NotApproved,
                            error.Message);
                        break;

                    case Constants.DeviceBlockedStatusCode:
                        _settings.DeviceStatus = new DeviceStatus(
                            DeviceStatusType.Blocked,
                            error.Message);
                        break;

                    default:
                        if (error.StatusCode == 404 && error.Message == Constants.ObjectNotFoundMessage)
                        {
                            if (this is RegisterTask)
                            {
                                Utils.ShowError(Utils.GetStringFromResource("RegisterErrorMessage"));
                            }
                            else
                            {
                                Utils.ShowError(Utils.GetStringFromResource("CommonNetworkErrorMessage"));
                            }
                        }
                        else
                        {
                            Utils.ShowError(error.Message);
                        }
                        break;
                }
            }
        }

        public void Cancel()
        {
            IsCanceled = true;
            CancelInternal();
        }
    }
}
