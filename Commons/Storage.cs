﻿using Quest.Cyclone.Client.Windows.Commons.Interfaces;

using Windows.Storage;

namespace Quest.Cyclone.Client.Windows.Commons
{
    public class Storage : IStorage
    {
        public void SaveSetting<T>(string name, T value)
        {
            var settings = ApplicationData.Current.LocalSettings;
            settings.Values[name] = value;
        }

        public T GetSetting<T>(string name)
        {
            object value;
            if (ApplicationData.Current.LocalSettings.Values.TryGetValue(name, out value))
            {
                return (T)value;
            }

            return default(T);
        }
    }
}
