﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Caliburn.Micro;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public enum AlertStatus
    {
        New = 0,
        Acknowledged,
        Resolved
    }

    public enum AlertSeverity
    {
        Information = 0,
        Minor,
        Major,
        Critical
    }

    public class AlertBriefDescription : PropertyChangedBase
    {
        [XmlAttribute]
        public string Title { get; set; }

        [XmlAttribute]
        public string Description { get; set; }

        [XmlAttribute]
        public AlertSeverity Severity { get; set; }

        private AlertStatus _status;

        [XmlAttribute]
        public AlertStatus Status
        {
            get { return _status; }
            set
            {
                _status = value;
                NotifyOfPropertyChange(() => Status);
            }
        }

        private RunState _state;

        [XmlAttribute]
        public RunState State
        {
            get { return _state; }
            set
            {
                _state = value;
                NotifyOfPropertyChange(() => State);
            }
        }

        [XmlAttribute]
        public string ResultDescription { get; set; }

        private string _owner;

        [XmlAttribute]
        public string Owner
        {
            get { return _owner; }
            set
            {
                _owner = value;
                NotifyOfPropertyChange(() => Owner);
            }
        }

        private string _source;

        [XmlAttribute]
        public string Source
        {
            get { return _source; }
            set
            {
                _source = value;
                NotifyOfPropertyChange(() => Source);
            }
        }

        private long _time;

        [XmlAttribute]
        public long Time
        {
            get { return _time; }
            set
            {
                _time = value;
                NotifyOfPropertyChange(() => Time);
            }
        }
    }

    public enum CustomFieldType
    {
        HTML = 0,
        Text
    }

    public class CustomField : PropertyChangedBase
    {
        [XmlAttribute]
        public string Name { get; set; }

        [XmlText]
        public string Value { get; set; }

        [XmlAttribute]
        public CustomFieldType Type { get; set; }

        public CustomField()
        {
            Type = CustomFieldType.Text;
        }

        public override string ToString()
        {
            return String.Format("{0}({1}:{2}, {3})", GetType().Name, Type, Name, Value);
        }
    }

    public class AlertFullDescription : PropertyChangedBase
    {
        private ActionDefinition[] _actions;

        [XmlArray("Actions")]
        [XmlArrayItem("Action")]
        public ActionDefinition[] Actions
        {
            get { return _actions; }
            set
            {
                _actions = value;
                NotifyOfPropertyChange(() => Actions);
            }
        }

        public CustomField[] CustomFields { get; set; }
    }

    public class AlertId : PropertyChangedBase
    {
        [XmlAttribute]
        public Guid Id { get; set; }

        public override string ToString()
        {
            return String.Format("{0}({1})", GetType().Name, Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Id == ((AlertId)obj).Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }

    public class Alert : AlertId
    {
        public Alert()
        {
            _historyItems = new List<HistoryItem>();    
        }

        [XmlElement("Brief")]
        public AlertBriefDescription Brief { get; set; }

        private AlertFullDescription _full;

        [XmlElement("Full")]
        public AlertFullDescription Full
        {
            get { return _full; }
            set
            {
                _full = value;
                NotifyOfPropertyChange(() => Full);
            }
        }

        private List<HistoryItem> _historyItems;

        [XmlArrayItem("HistoryItem")]
        public List<HistoryItem> HistoryItems
        {
            get { return _historyItems; }
            set
            {
                _historyItems = value;
                NotifyOfPropertyChange(() => HistoryItems);
            }
        }

        private bool _isRemoved;

        [XmlIgnore]
        public bool IsRemoved
        {
            get { return _isRemoved; }
            set
            {
                _isRemoved = value;
                NotifyOfPropertyChange(() => IsRemoved);
            }
        }

        public void UpdateFrom(Alert alert)
        {
            if (Brief.Owner != alert.Brief.Owner)
                Brief.Owner = alert.Brief.Owner;

            if (Brief.ResultDescription != alert.Brief.ResultDescription)
                Brief.ResultDescription = alert.Brief.ResultDescription;

            if (Brief.Severity != alert.Brief.Severity)
                Brief.Severity = alert.Brief.Severity;

            if (Brief.State != alert.Brief.State)
                Brief.State = alert.Brief.State;

            if (Brief.Status != alert.Brief.Status)
                Brief.Status = alert.Brief.Status;

            if (Brief.Time != alert.Brief.Time)
                Brief.Time = alert.Brief.Time;

            if (Brief.Title != alert.Brief.Title)
                Brief.Title = alert.Brief.Title;

            if (Full == null && alert.Full != null)
                Full = alert.Full;

            if (HistoryItems == null)
            {
                HistoryItems = alert.HistoryItems;
            }
            else if (HistoryItems != null && alert.HistoryItems.Any())
            {
                var items = new List<HistoryItem>(HistoryItems);
                items.AddRange(alert.HistoryItems);
                items.Sort((first, second) => second.Time.CompareTo(first.Time));
                HistoryItems = items.Distinct(new HistoryItemComparer()).ToList();
            }
        }

        public override string ToString()
        {
            var title = Brief == null ? "<null brief description>" : Brief.Title;
            return String.Format("{0}({1}, {2})", GetType().Name, Id, title);
        }
    }

    [XmlRoot("Alerts")]
    public class Alerts
    {
        public Alerts()
        {
            Items = new List<Alert>();
        }

        [XmlElement("Alert")]
        public List<Alert> Items { get; set; }
    }
}