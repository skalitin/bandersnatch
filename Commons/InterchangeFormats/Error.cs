namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public class Error
    {
        public string Message { get; set; }
        public string Details { get; set; }
    }
}


