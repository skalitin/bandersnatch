﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public class UserCredentials
    {
        public UserCredentials()
        {
        }

        public UserCredentials(string domain, string login, string password)
        {
            Domain = domain;
            Login = login;
            Password = password;
        }

        [XmlAttribute]
        public string Domain { get; set; }

        [XmlAttribute]
        public string Login { get; set; }

        [XmlAttribute]
        public string Password { get; set; }

        public override string ToString()
        {
            return String.IsNullOrEmpty(Domain) ? Login : String.Format("{0}\\{1}", Domain, Login);
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
            {
                return true;
            }

            var other = obj as UserCredentials;
            if (other == null)
            {
                return false;
            }

            return Domain == other.Domain && Login == other.Login && Password == other.Password;
        }

        public override int GetHashCode()
        {
            return (Domain == null ? 0 : Domain.GetHashCode()) ^
                   (Login == null ? 0 : Login.GetHashCode()) ^
                   (Password == null ? 0 : Password.GetHashCode());
        }

        public NetworkCredential ToNetworkCredentials()
        {
            return new NetworkCredential(Login, Password, Domain);
        }

        public UserCredentials Clone()
        {
            return new UserCredentials(Domain, Login, Password);
        }
    }
}
