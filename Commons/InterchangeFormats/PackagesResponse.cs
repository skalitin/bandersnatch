﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    [XmlRoot("Packages")]
    public class PackagesResponse
    {
        public PackagesResponse()
        {
            Packages = new List<Package>();
        }

        [XmlAttribute]
        public long SyncId { get; set; }

        [XmlElement("Package")]
        public List<Package> Packages { get; set; }
    }
}
