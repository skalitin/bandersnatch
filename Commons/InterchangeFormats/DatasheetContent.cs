﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Caliburn.Micro;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public class DatasheetContent
    {
        public DatasheetContent()
        {
            Columns = new DatasheetColumn[] { };
            Records = new DatasheetRecord[] { };
            ActionCategories = new DatasheetActionCategory[] { };
        }

        public bool DisableRecordDetailsScreen { get; set; }

        [XmlArray("Columns")]
        [XmlArrayItem("Column")]
        public DatasheetColumn[] Columns { get; set; }

        [XmlArray("Records")]
        [XmlArrayItem("Record")]
        public DatasheetRecord[] Records { get; set; }

        [XmlArray("ActionCategories")]
        [XmlArrayItem("ActionCategory")]
        public DatasheetActionCategory[] ActionCategories { get; set; }
    }
}
