﻿using System;
using System.Xml.Serialization;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public class DatasheetAction
    {
        public DatasheetAction()
        {
            Id = Guid.Empty;
        }

        [XmlAttribute]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Icon Icon { get; set; }
    }
}