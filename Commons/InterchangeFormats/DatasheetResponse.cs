﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public enum DatasheetResponseType
    {
        Datasheets,
        DatasheetContent,
        Parameters,
        Relogon
    }

    public enum DatasheetOperationState
    {
        Running = 0,
        Completed = 1,
        Aborted = 2,
        SessionClosed = 3
    }

    [XmlRoot(ElementName = "Response")]
    public class DatasheetResponse
    {
        [XmlElement("RelogonInformation")]
        public RelogonInformation RelogonInformation { get; set; }

        [XmlElement("DatasheetContent")]
        public DatasheetContent Content { get; set; }

        [XmlArray(ElementName = "Datasheets")]
        [XmlArrayItem(ElementName = "Datasheet")]
        public Datasheet[] Datasheets { get; set; }

        [XmlArray(ElementName = "Messages")]
        [XmlArrayItem(ElementName = "Message")]
        public DatasheetMessage[] Messages { get; set; }

        [XmlElement("Parameters")]
        public DatasheetParameterGroup ParameterGroup { get; set; }

        [XmlAttribute("ResponseType")]
        public DatasheetResponseType ResponseType { get; set; }

        [XmlAttribute("OperationState")]
        public DatasheetOperationState OperationState { get; set; }

        public override string ToString()
        {
            return String.Format("{0}({1}, {2}, {3}, {4}, {5}, {6})",
                GetType().Name,
                ResponseType,
                OperationState,
                Datasheets == null ? "no datasheets" : String.Format("{0} datasheet(s)", Datasheets.Length),
                Content == null ? "no content" : Content.ToString(),
                ParameterGroup == null ? "no parameters" : ParameterGroup.ToString(),
                Messages == null ? "no messages" : String.Format("{0} message(s)", Messages.Length));
        }
    }
}
