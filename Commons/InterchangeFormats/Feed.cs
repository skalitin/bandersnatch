﻿using System.Xml.Serialization;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public enum FeedItemType
    {
        AlertCreated,
        AlertStatusChanged,
        AlertOwnerChanged,
        AlertActionStarted,
        AlertActionFinished
    }

    public class FeedItemProperty
    {
        public FeedItemProperty()
        {
        }

        public FeedItemProperty(string name, object value)
        {
            Name = name;
            Value = value == null ? "" : value.ToString();
        }

        [XmlAttribute]
        public string Name { get; set; }

        [XmlText]
        public string Value { get; set; }

        public const string AlertId = "AlertId";
        public const string AlertName = "AlertName";
    }

    public class FeedItem
    {
        [XmlAttribute]
        public long Time { get; set; }

        [XmlAttribute]
        public string User { get; set; }

        [XmlAttribute]
        public FeedItemType Type { get; set; }

        public string Comment { get; set; }
        
        [XmlArrayItem(ElementName = "Property")]
        public FeedItemProperty[] Properties { get; set; }
    }

    [XmlRoot(ElementName = "FeedItems")]
    public class Feed
    {
        [XmlElement(ElementName = "FeedItem")]
        public FeedItem[] Items { get; set; }
    }
}
