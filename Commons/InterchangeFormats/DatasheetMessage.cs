﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public enum DatasheetMessageSeverity
    {
        Information,
        Warning,
        Error
    }

    public class DatasheetMessage
    {
        public string Text { get; set; }
        public DatasheetMessageSeverity Severity { get; set; }
    }
}
