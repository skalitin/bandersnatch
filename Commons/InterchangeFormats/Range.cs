﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public class Range
    {
        public Range()
        {
            From = 0;
            To = 0;
        }

        public Range(long from, long to)
        {
            From = from;
            To = to;
        }

        public override string ToString()
        {
            return String.Format("{0}({1}..{2})", GetType().Name, From, To);
        }

        public long From { get; set; }
        public long To { get; set; }
    }
}
