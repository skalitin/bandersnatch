﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    [XmlRoot("Parameters")]
    public class DatasheetParameterGroup
    {
        [XmlElement("StringParameter", typeof(StringDatasheetParameter))]
        [XmlElement("SecureStringParameter", typeof(SecureStringDatasheetParameter))]
        [XmlElement("StaticTextParameter", typeof(StaticTextDatasheetParameter))]
        [XmlElement("IntegerParameter", typeof(IntegerDatasheetParameter))]
        [XmlElement("BooleanParameter", typeof(BooleanDatasheetParameter))]
        [XmlElement("SwitchParameter", typeof(SwitchDatasheetParameter))]
        [XmlElement("ArrayParameter", typeof(ArrayDatasheetParameter))]
        public DatasheetParameter[] Items
        {
            get
            {
                return _items ?? new DatasheetParameter[] { };
            }
            set
            {
                _items = value ?? new DatasheetParameter[] { };
            }
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Icon Icon { get; set; }
        public bool IsCancelled { get; set; }

        public override string ToString()
        {
            return String.Format("{0}({1}, {2} parameter(s){3})",
                GetType().Name,
                Name,
                Items.Length,
                IsCancelled ? ", cancelled" : "");
        }

        private DatasheetParameter[] _items;
    }
}
