﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    [XmlRoot("Package")]
    public class PackageUpdates
    {
        public PackageUpdates()
        {
            Alerts = new List<Alert>();
            Removed = new List<AlertId>();
            FeedItems = new List<FeedItem>();
            Instances = new List<Instance>();
            RemovedInstances = new List<Instance>();
        }

        [XmlAttribute]
        public Guid Id { get; set; }

        [XmlAttribute(AttributeName = "Badge")]
        public int Badge { get; set; }

        [XmlArray]
        public List<Alert> Alerts { get; set; }

        [XmlArray(ElementName = "Removed")]
        [XmlArrayItem(ElementName = "Alert")]
        public List<AlertId> Removed { get; set; }

        [XmlArray(ElementName = "FeedItems")]
        [XmlArrayItem(ElementName = "FeedItem")]
        public List<FeedItem> FeedItems { get; set; }

        [XmlArray]
        public List<Instance> Instances { get; set; }

        [XmlArray]
        public List<Instance> RemovedInstances { get; set; }
    }

    [XmlRoot("Updates")]
    public class UpdatesResponse
    {
        public UpdatesResponse()
        {
            Packages = new List<PackageUpdates>();
        }
        [XmlAttribute]
        public long SyncId { get; set; }

        [XmlElement("Package")]
        public List<PackageUpdates> Packages { get; set; }

        public override string ToString()
        {
            return String.Format("{0}(SyncId: {1})", GetType().Name, SyncId);
        }
    }
}
