﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Caliburn.Micro;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public enum HistoryItemType
    {
        Created,
        StatusChanged,
        OwnerChanged,
        ActionStarted,
        ActionFinished
    }

    public class HistoryItemProperty
    {
        public const string Owner = "Owner";
        public const string Status = "Status";
        public const string ActionId = "ActionId";
        public const string ActionName = "ActionName";
        public const string Success = "Success";

        public HistoryItemProperty()
        {
        }

        public HistoryItemProperty(string name, object value)
        {
            Name = name;
            Value = value == null ? "" : value.ToString();            
        }

        [XmlAttribute]
        public string Name { get; set; }
        
        [XmlText]
        public string Value { get; set; }
    }

    public class HistoryItem : PropertyChangedBase
    {
        [XmlAttribute]
        public long Time { get; set; }

        [XmlAttribute]
        public HistoryItemType Type { get; set; }

        [XmlAttribute]
        public string User { get; set; }

        public string Comment { get; set; }
        
        [XmlArrayItem(ElementName = "Property")]
        public HistoryItemProperty[] Properties { get; set; }

        public FeedItem AsFeedItem(Alert alert)
        {
            var typeMapping = new Dictionary<HistoryItemType, FeedItemType> {
                { HistoryItemType.Created,          FeedItemType.AlertCreated },
                { HistoryItemType.StatusChanged,    FeedItemType.AlertStatusChanged },
                { HistoryItemType.OwnerChanged,     FeedItemType.AlertOwnerChanged },
                { HistoryItemType.ActionStarted,    FeedItemType.AlertActionStarted },
                { HistoryItemType.ActionFinished,   FeedItemType.AlertActionFinished }
            };

            var type = typeMapping[Type];
            var properties = (from property in Properties select new FeedItemProperty(property.Name, property.Value)).ToList();
            properties.Add(new FeedItemProperty(FeedItemProperty.AlertId, alert.Id));
            properties.Add(new FeedItemProperty(FeedItemProperty.AlertName, alert.Brief.Title));

            return new FeedItem
            {
                Time = Time,
                User = User,
                Type = type,
                Comment = Comment,
                Properties = properties.ToArray()
            };
        }
    }
}