﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Caliburn.Micro;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public class Datasheet
    {
        public Datasheet()
        {
            Id = Guid.Empty;
            IsExpandable = false;
            HasRecords = true;
            HasDynamicActions = false;
        }

        [XmlAttribute]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Icon Icon { get; set; }
        public bool IsExpandable { get; set; }
        public bool HasRecords { get; set; }
        public bool HasDynamicActions { get; set; }
    }
}
