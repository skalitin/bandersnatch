﻿using System.Xml.Serialization;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    [XmlRoot("Version")]
    public class ServerVersion
    {
        [XmlAttribute(AttributeName = "File")]
        public string FileVersion { get; set; }

        [XmlAttribute(AttributeName = "Assembly")]
        public string AssemblyVersion { get; set; }
    }
}
