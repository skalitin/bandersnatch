﻿using System;
using System.Xml.Serialization;
using Caliburn.Micro;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public enum AvailableContentType
    {
        All,
        AlertsOnly,
        ReportsOnly
    }

    public class Package : PropertyChangedBase
    {
        private Icon _icon;
        private int _badge;

        public Package()
        {
            Title = string.Empty;
            Instances = new BindableCollection<Instance>();
        }

        [XmlAttribute]
        public Guid Id { get; set; }

        [XmlAttribute]
        public string Title { get; set; }

        public Icon Icon
        {
            get { return _icon; }
            set
            {
                _icon = value;
                NotifyOfPropertyChange(() => Icon);
            }
        }

        [XmlAttribute]
        public AvailableContentType AvailableContentType { get; set; }

        [XmlAttribute("Badge")]
        public int Badge
        {
            get { return _badge; }
            set
            {
                _badge = value;
                NotifyOfPropertyChange(() => Badge);
            }
        }

        [XmlArray]
        public BindableCollection<Instance> Instances { get; set; }
    }
}
