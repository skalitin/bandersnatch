﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    [XmlRoot("Icon")]
    public class Icon
    {
        private string _id;
        private byte[] _data;

        public Icon()
        {
        }

        public Icon(byte[] data)
        {
            Data = data;
        }

        [XmlElement]
        public byte[] Data
        {
            get { return _data; }
            set { _data = value; }
        }

        [XmlAttribute]
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public void ClearData()
        {
            _data = null;
        }

        public bool HasId()
        {
            return Id != null;
        }

        public bool HasData()
        {
            return Data != null;
        }

        public override string ToString()
        {
            return String.Format("{0}(id: {1}, data: {2})", GetType().Name, _id, _data);
        }
    }
}
