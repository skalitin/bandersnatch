﻿using System;
using System.Xml.Serialization;
using Caliburn.Micro;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public enum ActionType
    {
        Alert,
        Report,
        Package
    }

    public enum ResultType
    {
        Success,
        Failure
    }

    public enum RunState
    {
        NotStarted,
        InProgress, 
        Success, 
        Failure
    }

    public class ActionDefinition : PropertyChangedBase
    {
        [XmlAttribute]
        public Guid Id { get; set; }

        [XmlAttribute]
        public ActionType Type { get; set; }

        [XmlAttribute]
        public string Title { get; set; }

        public string Description { get; set; }

        [XmlAttribute]
        public Guid SubjectId { get; set; }

        [XmlAttribute]
        public bool CredentialsRequired { get; set; }

        [XmlAttribute]
        public string Internal { get; set; }
    }
}
