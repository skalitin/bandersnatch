﻿using System;
using System.Xml.Serialization;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public enum RelogonType
    {
        Manual,
        Automatic
    }

    [XmlRoot(ElementName = "RelogonInformation")]
    public class RelogonInformation
    {
        public UserCredentials UserCredentials { get; set; }
        public RelogonType RelogonType { get; set; }
        public string PreviousUser { get; set; }

        public override string ToString()
        {
            return String.Format("{0}(user '{1}', relogon type '{2}')", GetType().Name, UserCredentials, RelogonType);
        }
    }
}