﻿using System.Xml.Serialization;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public class DatasheetActionCategory
    {
        public DatasheetActionCategory()
        {
            ShowHeader = true;
        }

        [XmlArray(ElementName = "Actions")]
        [XmlArrayItem(ElementName = "Action")]
        public DatasheetAction[] Actions { get; set; }

        public string Name { get; set; }
        public bool ShowHeader { get; set; }
    }
}