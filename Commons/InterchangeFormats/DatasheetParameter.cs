﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Caliburn.Micro;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    [KnownType(typeof(StringDatasheetParameter))]
    [KnownType(typeof(SecureStringDatasheetParameter))]
    [KnownType(typeof(StaticTextDatasheetParameter))]
    [KnownType(typeof(IntegerDatasheetParameter))]
    [KnownType(typeof(BooleanDatasheetParameter))]
    [KnownType(typeof(SwitchDatasheetParameter))]
    [KnownType(typeof(ArrayDatasheetParameter))]
    public class DatasheetParameter : PropertyChangedBase
    {
        public DatasheetParameter()
        {
        }

        public DatasheetParameter(string name)
        {
            Name = name;
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class StaticTextDatasheetParameter : StringDatasheetParameter
    {
        public StaticTextDatasheetParameter()
            : base()
        {
        }

        public StaticTextDatasheetParameter(string name)
            : base(name)
        {
        }

        public StaticTextDatasheetParameter(string name, string value)
            : base(name, value)
        {
        }
    }

    public class StringDatasheetParameter : DatasheetParameter
    {
        public StringDatasheetParameter()
            : base()
        {
        }

        public StringDatasheetParameter(string name)
            : base(name)
        {
        }

        public StringDatasheetParameter(string name, string value)
            : base(name)
        {
            Value = value;
        }

        public string Value { get; set; }
    }

    public class SecureStringDatasheetParameter : StringDatasheetParameter
    {
        public SecureStringDatasheetParameter()
            : base()
        {
        }

        public SecureStringDatasheetParameter(string name)
            : base(name)
        {
        }
    }

    public class IntegerDatasheetParameter : DatasheetParameter
    {
        public IntegerDatasheetParameter()
            : base()
        {
        }

        public IntegerDatasheetParameter(string name)
            : base(name)
        {
        }

        public IntegerDatasheetParameter(string name, int value)
            : base(name)
        {
            Value = value;
        }

        public int Value { get; set; }
    }

    public class BooleanDatasheetParameter : DatasheetParameter
    {
        public BooleanDatasheetParameter()
            : base()
        {
        }

        public BooleanDatasheetParameter(string name)
            : base(name)
        {
            Value = false;
        }

        public BooleanDatasheetParameter(string name, Boolean value)
            : base(name)
        {
            Value = value;
        }

        public Boolean Value { get; set; }
    }

    public class SwitchDatasheetParameter : DatasheetParameter
    {
        public SwitchDatasheetParameter()
            : base()
        {
            _hasDefaultValue = true;
            EffectiveValues = new BindableCollection<string>();
        }

        public SwitchDatasheetParameter(string name, IEnumerable<string> values)
            : base(name)
        {
            EffectiveValues = new BindableCollection<string>();
            Values = values.ToArray();
            _hasDefaultValue = true;
        }

        public SwitchDatasheetParameter(string name, IEnumerable<string> values, int index)
            : base(name)
        {
            Index = index;
            EffectiveValues = new BindableCollection<string>();
            Values = values.ToArray();
            _hasDefaultValue = true;
        }

        private bool _hasDefaultValue;

        private int _index;
        public int Index
        {
            get
            {
                return _index;
            }
            set
            {
                _index = value;
                if (_index < 0)
                {
                    _hasDefaultValue = false;
                    EffectiveIndex = 0;
                }
                else
                    EffectiveIndex = _index;
                NotifyOfPropertyChange(() => Index);
            }
        }

        private int _effectiveIndex;
        public int EffectiveIndex
        {
            get { return _effectiveIndex; }
            set
            {
                _effectiveIndex = value;
                if (_hasDefaultValue)
                    _index = value;
                else
                    _index = value - 1;
                NotifyOfPropertyChange(() => EffectiveIndex);
                NotifyOfPropertyChange(() => EffectiveValue);
            }
        }

        private string[] _values;

        [XmlArray("Values")]
        [XmlArrayItem("Value")]
        public string[] Values
        {
            get { return _values; }
            set
            {
                _values = value;
                if (!_hasDefaultValue)
                    EffectiveValues.Add(" ");

                EffectiveValues.AddRange(value);

                NotifyOfPropertyChange(() => Values);
                NotifyOfPropertyChange(() => HasManyItems);
            }
        }

        [XmlIgnore]
        public BindableCollection<string> EffectiveValues { get; set; }

        [XmlIgnore]
        public string EffectiveValue
        {
            get { return EffectiveValues[EffectiveIndex]; }
        }

        [XmlIgnore]
        public bool HasManyItems
        {
            get { return Values.Length > 5; }
        }
    }

    public class ArrayDatasheetParameter : DatasheetParameter
    {
        public ArrayDatasheetParameter()
            : base()
        {
        }

        public ArrayDatasheetParameter(string name, ICollection<string> values)
            : base(name)
        {
            Values = values.ToArray();
        }

        private string[] _values;

        [XmlArray("Values")]
        [XmlArrayItem("Value")]
        public string[] Values
        {
            get { return _values; }
            set
            {
                _values = value;
                NotifyOfPropertyChange(() => Values);
            }
        }
    }
}
