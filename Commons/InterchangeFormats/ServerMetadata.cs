﻿using System.Xml.Serialization;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    [XmlRoot("ServerMetadata")]
    public class ServerMetadata
    {
        [XmlElement("ServerVersion")]
        public ServerVersion ServerVersion { get; set; }

        [XmlElement("HasAnonymousPack")]
        public bool HasAnonymousPack { get; set; }
    }
}
