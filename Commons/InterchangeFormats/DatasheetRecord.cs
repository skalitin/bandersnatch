﻿using System.Xml.Serialization;
using Caliburn.Micro;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public class DatasheetRecord : PropertyChangedBase
    {
        [XmlArray("Cells")]
        [XmlArrayItem("Cell")]
        public string[] Cells { get; set; }
        
        private Icon _icon;
        public Icon Icon
        {
            get { return _icon; }
            set
            {
                _icon = value;
                NotifyOfPropertyChange(() => Icon);
            }
        }
    }
}