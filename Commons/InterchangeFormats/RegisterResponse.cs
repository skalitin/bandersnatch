﻿using System.Xml.Serialization;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    [XmlRoot("RegisterResponse")]
    public class RegisterResponse
    {
        [XmlAttribute]
        public string Password { get; set; }
    }
}
