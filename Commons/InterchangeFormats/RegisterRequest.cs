﻿using System;
using System.Xml.Serialization;

namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public class DeviceProperty
    {
        [XmlAttribute]
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class RegisterRequest
    {
        [XmlAttribute]
        public string DeviceId { get; set; }

        [XmlAttribute]
        public string DeviceType { get; set; }
        
        public string DeviceName { get; set; }

        [XmlArray(ElementName = "Properties")]
        [XmlArrayItem(ElementName = "Property")]
        public DeviceProperty[] Properties { get; set; }
        
        public override string ToString()
        {
            return String.Format("{0}({1}, {2}, {3}, {4} properties)",
                GetType().Name,
                DeviceType,
                DeviceName,
                DeviceId,
                Properties == null ? 0 : Properties.Length);
        }
    }
}
