﻿namespace Quest.Cyclone.Client.Windows.Commons.InterchangeFormats
{
    public class DatasheetColumn
    {
        public DatasheetColumn()
        {
            IsPrimary = false;
        }

        public string Name { get; set; }
        public bool IsPrimary { get; set; }
    }
}