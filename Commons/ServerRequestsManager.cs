﻿using System;
using System.IO;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Xml.Serialization;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Windows.System.Profile;

namespace Quest.Cyclone.Client.Windows.Commons
{
    public class ServerRequestsManager : IServerRequestsManager
    {
        private readonly ISettings _settings;

        public ServerRequestsManager(ISettings seetings)
        {
            _settings = seetings;
        }

        public RegisterResponse RegisterDevice()
        {
            string body;
            var request = new RegisterRequest
            {
                DeviceId = GetHardwareDeviceId(),
                DeviceName = "WinRT Device",
                DeviceType = Constants.DeviceType
            };

            var serializer = new XmlSerializer(request.GetType());
            using (var memoryStream = new MemoryStream())
            {
                serializer.Serialize(memoryStream, request);
                body = Utils.MemoryStreamToString(memoryStream);
            }

            var response = NetworkConnector.PostRequest("register", body, _settings.UserName, _settings.UserPassword);
            return Deserialize<RegisterResponse>(response);
        }

        public void UnsubscribeDevice(string username, string usertoken)
        {
            NetworkConnector.PostRequest("unsubscribe", "", username, usertoken);
        }

        public ServerVersion GetServerVersion()
        {
            var response = NetworkConnector.GetRequest("version");
            return Deserialize<ServerVersion>(response);
        }

        public ServerMetadata GetServerMetadata()
        {
            var response = NetworkConnector.GetRequest("servermetadata");
            return Deserialize<ServerMetadata>(response);
        }

        public PackagesResponse GetPackages()
        {
            var response = NetworkConnector.GetRequest("packages");
            return Deserialize<PackagesResponse>(response);
        }

        public Package GetPackage(Guid packageId)
        {
            var url = string.Format("package?id={0}", packageId);
            var response = NetworkConnector.GetRequest(url);
            return Deserialize<Package>(response);
        }

        public Alerts GetAlerts(Guid packageId, AlertStatus status, int count, long upto)
        {
            var url = string.Format("alerts?packageId={0}&status={1}&count={2}&upto={3}", packageId, status, count, upto);
            var response = NetworkConnector.GetRequest(url);
            return Deserialize<Alerts>(response);
        }

        public byte[] GetImageData(string imageId)
        {
            return NetworkConnector.GetByteRequestAnonymously(String.Format("imageservice?id={0}", imageId));
        }

        public UpdatesResponse GetUpdates(long syncId)
        {
            var url = string.Format("updates?lastsync={0}", syncId);
            var response = NetworkConnector.GetRequest(url);
            return Deserialize<UpdatesResponse>(response);
        }

        public Alert GetAlertDetails(Guid alertId)
        {
            var url = string.Format("alertsdetails?id={0}", alertId);
            var response = NetworkConnector.GetRequest(url);
            var alerts = Deserialize<Alerts>(response);

            if (alerts.Items.Count > 0)
            {
                return alerts.Items[0];
            }

            return null;
        }

        public void AcknowledgeAlert(Guid alertId)
        {
            var url = string.Format("acknowledge?alertId={0}&description=", alertId);
            NetworkConnector.PostRequest(url, "");
        }

        public void ResolveAlert(Guid alertId, string comment)
        {
            var url = string.Format("resolve?alertId={0}&description={1}", alertId, WebUtility.UrlEncode(comment));
            NetworkConnector.PostRequest(url, "");
        }

        public void RunAlertAction(Guid actionId)
        {
            var url = string.Format("runaction?actionId={0}", actionId);
            NetworkConnector.PostRequest(url, "");
        }

        public DatasheetResponse GetDatasheets(Guid instanceId, Guid datasheetId, bool refresh)
        {
            var url = String.Format("datasheets?instanceId={0}&parentId={1}&refresh={2}", instanceId, datasheetId, refresh);
            var response = NetworkConnector.GetRequest(url);
            return Deserialize<DatasheetResponse>(response);
        }

        public DatasheetResponse GetDatasheetContent(Guid instanceId, Guid datasheetId, Range range, bool refresh)
        {
            var url = String.Format("datasheetcontent?instanceId={0}&datasheetId={1}&from={2}&to={3}&refresh={4}", instanceId, datasheetId, range.From, range.To, refresh);
            var response = NetworkConnector.GetRequest(url);
            return Deserialize<DatasheetResponse>(response);
        }

        public DatasheetResponse GetDatasheetActions(Guid instanceId, Guid datasheetId, int index)
        {
            var url = String.Format("datasheetactions?instanceId={0}&datasheetId={1}&rowIndexes={2}", instanceId, datasheetId, index);
            var response = NetworkConnector.GetRequest(url);
            return Deserialize<DatasheetResponse>(response);
        }

        public DatasheetResponse RunDatasheetAction(Guid instanceId, Guid datasheetId, Guid actionId, int index)
        {
            var url = String.Format("datasheetaction?instanceId={0}&datasheetId={1}&actionId={2}&rowIndexes={3}", instanceId, datasheetId, actionId, index);
            var response = NetworkConnector.PostRequest(url, "");
            return Deserialize<DatasheetResponse>(response);
        }

        public DatasheetResponse SetDatasheetParameters(Guid instanceId, Guid datasheetId, DatasheetParameterGroup parameters)
        {
            var url = String.Format("datasheetparameters?instanceId={0}&datasheetId={1}", instanceId, datasheetId);

            string body = null;
            var serializer = new XmlSerializer(typeof(DatasheetParameterGroup));
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, parameters, new XmlSerializerNamespaces());
                body = writer.ToString().Replace("utf-16", "utf-8");
            }

            var response = NetworkConnector.PostRequest(url, body);
            return Deserialize<DatasheetResponse>(response);
        }

        private T Deserialize<T>(string response) where T : class
        {
           var serializer = new XmlSerializer(typeof(T));
            using (var memoryStream = new StringReader(response))
            {
                return (T)serializer.Deserialize(memoryStream);
            }
        }

        private string GetHardwareDeviceId()
        {
            // For another way to get ID and other properties see http://studioshorts.com/blog/2012/10/windows-8-device-property-ids-device-enumeration-pnpobject/
            var token = HardwareIdentification.GetPackageSpecificToken(null);
            var stream = token.Id.AsStream();
            using (var reader = new BinaryReader(stream))
            {
                var bytes = reader.ReadBytes((int)stream.Length);
                return BitConverter.ToString(bytes);
            }
        }
    }
}
