﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;

namespace Quest.Cyclone.Client.Windows.Commons.Models
{
    public class DatasheetContentModel : PropertyChangedBase
    {
        public DatasheetContentModel()
            :this(new DatasheetContent())
        {
        }

        public DatasheetContentModel(DatasheetContent datasheetContent)
        {
            Columns = datasheetContent.Columns;
            Records = datasheetContent.Records;
            ActionCategories = datasheetContent.ActionCategories;
            DisableRecordDetailsScreen = datasheetContent.DisableRecordDetailsScreen;
        }

        public bool DisableRecordDetailsScreen { get; set; }
        public DatasheetColumn[] Columns { get; set; }

        private DatasheetRecord[] _records;
        public DatasheetRecord[] Records
        {
            get { return _records; }
            set
            {
                _records = value;
                NotifyOfPropertyChange(() => Records);
            }
        }

        private DatasheetActionCategory[] _actionCategories;
        public DatasheetActionCategory[] ActionCategories
        {
            get { return _actionCategories; }
            set
            {
                _actionCategories = value;
                NotifyOfPropertyChange(() => ActionCategories);
            }
        }
    }
}
