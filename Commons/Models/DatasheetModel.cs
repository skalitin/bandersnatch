﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;

namespace Quest.Cyclone.Client.Windows.Commons.Models
{
    public class DatasheetModel : PropertyChangedBase
    {
        public DatasheetModel()
            :this(new Datasheet())
        {
        }

        public DatasheetModel(Datasheet datasheet)
        {
            Id = datasheet.Id;
            Name = datasheet.Name;
            Description = datasheet.Description;
            IsExpandable = datasheet.IsExpandable;
            HasRecords = datasheet.HasRecords;
            HasDynamicActions = datasheet.HasDynamicActions;
            Icon = datasheet.Icon;
            DatasheetModels = new BindableCollection<DatasheetModel>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsExpandable { get; set; }
        public bool HasRecords { get; set; }
        public bool HasDynamicActions { get; set; }
        public BindableCollection<DatasheetModel> DatasheetModels { get; set; }

        private Icon _icon;
        public Icon Icon 
        {
            get { return _icon; }
            set
            {
                _icon = value; 
                NotifyOfPropertyChange(() => Icon);
            }
        }

        public void SetDatasheets(ICollection<Datasheet> datasheets)
        {
            var models = from Datasheet datasheet in datasheets select new DatasheetModel(datasheet);
            DatasheetModels.Clear();
            DatasheetModels.AddRange(models);
        }

        public DatasheetModel GetDatasheetModel(Guid datasheetId)
        {
            if (Id == datasheetId)
            {
                return this;
            }

            foreach (var each in DatasheetModels)
            {
                var datasheetModel = each.GetDatasheetModel(datasheetId);
                if (datasheetModel != null)
                {
                    return datasheetModel;
                }
            }

            return null;
        }
    }
}
