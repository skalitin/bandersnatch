﻿using System;
using System.Net;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons
{
    public class NetworkConnector
    {
        public static string PostRequest(string url, string body)
        {
            return PostRequest(url, body, IoC.Get<ISettings>().UserName, IoC.Get<ISettings>().UserToken);
        }

        public static string PostRequest(string url, string body, string user, string password)
        {
            if (string.IsNullOrEmpty(user) && string.IsNullOrEmpty(password))
            {
                user = Constants.AnonymousUserName;
            }

            var httpWebRequest = WebRequest.CreateHttp(Utils.GetDeviceApiUrl(IoC.Get<ISettings>().ServerName, IoC.Get<ISettings>().IsUsingCompanyName) + url);
            var httpWebRequestSync = new HttpWebRequestSync(httpWebRequest, body)
            {
                RawWebRequest =
                {
                    // Workaround for some WP7 bug: empty password doesn't work, 
                    // use explicit authorization header.
                    // Credentials = new NetworkCredential(  SettingsWorker.Instance.UserName, useUserToken ? SettingsWorker.Instance.UserToken : SettingsWorker.Instance.UserPassword),
                    UseDefaultCredentials = false,
                    Accept = "text/xml",
                    ContentType = "text/xml;charset=\"utf-8\""
                }
            };

            httpWebRequestSync.RawWebRequest.Headers["Authorization"] = Utils.GetBasicAuthorizationHeader(user, password);
            return httpWebRequestSync.SyncPost();
        }

        public static String GetRequest(string url)
        {
            return GetRequest(url, IoC.Get<ISettings>().UserName, IoC.Get<ISettings>().UserToken);
        }

        public static String GetRequest(string url, string user, string password)
        {
            if (string.IsNullOrEmpty(user))
            {
                user = Constants.AnonymousUserName;
            }

            if(url.Contains("?"))
                url += "&nocache=" + DateTime.Now.Ticks;
            else
                url += "?nocache=" + DateTime.Now.Ticks;

            var httpWebRequest = WebRequest.CreateHttp(Utils.GetDeviceApiUrl(IoC.Get<ISettings>().ServerName, IoC.Get<ISettings>().IsUsingCompanyName) + url);
            var httpWebRequestSync = new HttpWebRequestSync(httpWebRequest, null)
            {
                RawWebRequest =
                {
                    //Credentials = new NetworkCredential(user, password),
                    UseDefaultCredentials = false,
                    Accept = "text/xml",
                }
            };

            httpWebRequestSync.RawWebRequest.Headers["Authorization"] = Utils.GetBasicAuthorizationHeader(user, password);
            return httpWebRequestSync.SyncGet();
        }

        public static byte[] GetByteRequestAnonymously(String url)
        {
            var uriString = Utils.GetDeviceApiUrl(IoC.Get<ISettings>().ServerName, IoC.Get<ISettings>().IsUsingCompanyName) + url;
            var httpWebRequest = WebRequest.CreateHttp(uriString);
            var httpWebRequestSync = new HttpWebRequestSync(httpWebRequest, null);
            
            return httpWebRequestSync.SyncGetBytes();
        }
    }
}