﻿using System;
using System.Linq.Expressions;

namespace Quest.Cyclone.Client.Windows.Commons
{
    public class SettingMessage
    {
        private readonly object _oldValue;
        private readonly object _newValue;
        private readonly string _propertyName;

        public SettingMessage(string propertyName, object oldValue, object newValue)
        {
            _propertyName = propertyName;
            _oldValue = oldValue;
            _newValue = newValue;
        }

        public bool IsChanged
        {
            get { return !OldValue.Equals(NewValue); }
        }

        public object OldValue
        {
            get { return _oldValue; }
        }

        public object NewValue
        {
            get { return _newValue; }
        }

        public string PropertyName
        {
            get { return _propertyName; }
        }

        public bool Is<T>(Expression<Func<T>> expression)
        {
            return ((MemberExpression)(expression.Body)).Member.Name == PropertyName;
        }
    }
}
