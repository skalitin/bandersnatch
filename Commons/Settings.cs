﻿using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Action = System.Action;

namespace Quest.Cyclone.Client.Windows.Commons
{
    public enum DeviceStatusType
    {
        Approved,
        NotApproved,
        Blocked
    }

    public class DeviceStatus
    {
        protected bool Equals(DeviceStatus other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((DeviceStatus) obj);
        }

        public override int GetHashCode()
        {
            return (int) Value;
        }

        public DeviceStatus(DeviceStatusType status, string message = null)
        {
            Value = status;
            Message = message;
        }

        public DeviceStatusType Value { get; private set; }
        public string Message { get; private set; }

        public static bool operator != (DeviceStatus first, DeviceStatusType second)
        {
            return first != null && first.Value != second;
        }

        public static bool operator == (DeviceStatus first, DeviceStatusType second)
        {
            return !(first != second);
        }
    }

    public class Settings : ISettings
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly IStorage _storage;
        private DeviceStatus _deviceStatus;

        public Settings(IStorage storage, IEventAggregator eventAggregator)
        {
            _deviceStatus = new DeviceStatus(DeviceStatusType.Approved);
            _storage = storage;
            _eventAggregator = eventAggregator;
        }

        public bool IsUsingCompanyName
        {
            get
            {
                return _storage.GetSetting<bool>(Utils.GetPropertyName(() => IsUsingCompanyName));
            }
            set
            {
                ProceedPropertyChange(() => 
                    _storage.SaveSetting(Utils.GetPropertyName(() => IsUsingCompanyName), value), 
                    () => IsUsingCompanyName, 
                    value);
            }
        }

        public bool IsLoggedOn
        {
            get
            {
                return !string.IsNullOrEmpty(UserToken);
            }
        }

        public DeviceStatus DeviceStatus
        {
            get { return _deviceStatus; }
            set
            {
                ProceedPropertyChange(() =>
                   _deviceStatus = value,
                   () => DeviceStatus,
                   value);
            }
        }

        public bool IsAnonymousLogonMode
        {
            get
            {
                return string.IsNullOrEmpty(UserName) && string.IsNullOrEmpty(UserPassword);
            }
        }

        public string ServerName
        {
            get
            {
                return _storage.GetSetting<string>(Utils.GetPropertyName(() => ServerName));
            }
            set
            {
                ProceedPropertyChange(() =>
                    _storage.SaveSetting(Utils.GetPropertyName(() => ServerName), value),
                    () => ServerName,
                    value);
            }
        }

        public string UserName
        {
            get
            {
                return _storage.GetSetting<string>(Utils.GetPropertyName(() => UserName));
            }
            set
            {
                ProceedPropertyChange(() =>
                    _storage.SaveSetting(Utils.GetPropertyName(() => UserName), value),
                    () => UserName,
                    value);
            }
        }

        public string UserPassword
        {
            get
            {
                return _storage.GetSetting<string>(Utils.GetPropertyName(() => UserPassword));
            }
            set
            {
                ProceedPropertyChange(() =>
                    _storage.SaveSetting(Utils.GetPropertyName(() => UserPassword), value),
                    () => UserPassword,
                    value);
            }
        }

        public string UserToken
        {
            get
            {
                return _storage.GetSetting<string>(Utils.GetPropertyName(() => UserToken));
            }
            set
            {
                ProceedPropertyChange(() =>
                    _storage.SaveSetting(Utils.GetPropertyName(() => UserToken), value),
                    () => UserToken,
                    value);

                ProceedPropertyChange(() => IsLoggedOn);
            }
        }

        public string ServerVersion
        {
            get
            {
                return _storage.GetSetting<string>(Utils.GetPropertyName(() => ServerVersion));
            }
            set
            {
                ProceedPropertyChange(() =>
                    _storage.SaveSetting(Utils.GetPropertyName(() => ServerVersion), value),
                    () => ServerVersion,
                    value);
            }
        }

        public long SyncId
        {
            get
            {
                return _storage.GetSetting<long>(Utils.GetPropertyName(() => SyncId));
            }
            set
            {
                ProceedPropertyChange(() =>
                    _storage.SaveSetting(Utils.GetPropertyName(() => SyncId), value),
                    () => SyncId,
                    value);
            }
        }

        public string ApplicationVersion
        {
            get
            {
                var assemblyName = GetType().AssemblyQualifiedName;
                var match = new Regex("Version=(?<version>[0-9.]*)").Match(assemblyName);

                var version = "";
                if (match.Success)
                {
                    version = match.Groups["version"].Value;
                }
                return version;
            }
        }

        public TimeSpan UpdateInterval
        {
            get
            {
                var interval = _storage.GetSetting<TimeSpan>(Utils.GetPropertyName(() => UpdateInterval));
                if (interval == default(TimeSpan))
                {
                    interval = TimeSpan.FromSeconds(10);
                }

                return interval;
            }
            set
            {
                ProceedPropertyChange(() =>
                    _storage.SaveSetting(Utils.GetPropertyName(() => UpdateInterval), value),
                    () => UpdateInterval,
                    value);
            }
        }

        private void ProceedPropertyChange<TProperty>(Expression<Func<TProperty>> variable)
        {
            var value = Utils.GetPropertyValue(variable);

            _eventAggregator.Publish(new SettingMessage(Utils.GetPropertyName(variable), value, value));
        }

        private void ProceedPropertyChange<TProperty>(Action action, Expression<Func<TProperty>> variable,
                                                      TProperty newValue)
        {
            var oldValue = Utils.GetPropertyValue(variable);

            action();

            _eventAggregator.Publish(new SettingMessage(Utils.GetPropertyName(variable), oldValue, newValue));    
        }
    }
}