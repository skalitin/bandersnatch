﻿using System;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;
using Windows.ApplicationModel.Resources;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace Quest.Cyclone.Client.Windows.Commons
{
    public class UIManager : IUIManager
    {
        public void RunOnUI(System.Action action)
        {
            action.OnUIThread();
        }

        public void ShowErrorByResourceName(string resourceName)
        {
            throw new NotImplementedException();
        }

        public void ShowErrorByResourceNameAndFormat(string resourceName, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void ShowError(string message)
        {
            throw new NotImplementedException();
        }

        public void ShowError(Exception e)
        {
            throw new NotImplementedException();
        }

        public void ShowToast(string message)
        {
            var toastXmlString = 
                string.Format("<toast duration='long'>"
                + "<visual version='1'>"
                + "<binding template='ToastText01'>"
                + "<text id='1'>{0}"
                + "</text>"
                + "</binding>"
                + "</visual>"
                + "</toast>", message);

            var toastDom = new XmlDocument();
            toastDom.LoadXml(toastXmlString);

            var toast = new ToastNotification(toastDom);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        public string GetStringFromResource(string resourceId)
        {
            var loader = new ResourceLoader();
            return loader.GetString(resourceId);
        }
    }
}
