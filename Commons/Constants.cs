﻿using System;


namespace Quest.Cyclone.Client.Windows.Commons
{
    public class Constants
    {
        public const int NoStatusCode = -1;
        public const int DeviceNotApprovedStatusCode = 511;
        public const int DeviceBlockedStatusCode = 512;
        public const int UpdateSleepTime = 60000;
        public const int ContentRowPerRequestCount = 20;
        public const string MitServerName = "mobileit";
        public const string HttpPrefix = "https://";
        public const string DeviceType = "WindowsPhone";
        public const string ProxyServer = "mobileit.quest.com";
        public const string AnonymousUserName = "<anonymous>";
        public static readonly Guid RootDatasheetId = Guid.Empty;
        public const string ObjectNotFoundMessage = "Object not found";
        public const int MaximumAlertActionsCount = 6; // TODO WinRT doesn't support context menu with more than 6 elements
    }
}
