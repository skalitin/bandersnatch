﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;
using Quest.Cyclone.Client.Windows.Commons.Interfaces;

namespace Quest.Cyclone.Client.Windows.Commons
{
    public class IconDataRestorer : IIconDataRestorer
    {
        private readonly ConcurrentDictionary<string, byte[]> _iconDataById;
        private readonly IServerRequestsManager _serverRequestsManager;

        public IconDataRestorer(IServerRequestsManager serverRequestsManager)
        {
            _serverRequestsManager = serverRequestsManager;
            _iconDataById = new ConcurrentDictionary<string, byte[]>();
        }

        private byte[] GetIconData(Icon icon)
        {
            if (icon != null && !string.IsNullOrEmpty(icon.Id))
            {
                lock (_iconDataById)
                {
                    byte[] data;
                    if (_iconDataById.ContainsKey(icon.Id))
                    {
                        _iconDataById.TryGetValue(icon.Id, out data);
                        return data;
                    }

                    data = _serverRequestsManager.GetImageData(icon.Id);
                    _iconDataById[icon.Id] =  data;
                    return data;
                }
            }

            return null;
        }

        public Icon RestoreIconData(Icon icon)
        {
            var data = GetIconData(icon);
            if (data != null)
            {
                icon.Data = data;
            }
            return icon;
        }

        public Task<Icon> RestoreIconDataAsync(Icon icon)
        {
            return Task.Run(() => RestoreIconData(icon));
        }
    }
}
