﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;

namespace Quest.Cyclone.Client.Windows.Commons.Interfaces
{
    public interface IPackageStorage
    {
        void AddPackage(Package package);
        void AddPackages(ICollection<Package> packages);
        void SetPackages(ICollection<Package> packages);
        void RemovePackages(ICollection<Guid> ids);
        void RemovePackages();
        void RemovePackage(Guid id);
        void UpdatePackageBadgeCount(Guid packageId, int badge);
        BindableCollection<Package> Packages { get; }
        Package GetPackage(Guid packageId);
    }
}