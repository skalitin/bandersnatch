﻿using System;

namespace Quest.Cyclone.Client.Windows.Commons.Interfaces
{
    public interface ISettings
    {
        bool IsUsingCompanyName { get; set; }
        bool IsLoggedOn { get; }
        DeviceStatus DeviceStatus { get; set; }
        long SyncId { get; set; }
        bool IsAnonymousLogonMode { get; }
        string ServerName { get; set; }
        string UserName { get; set; }
        string UserPassword { get; set; }
        string UserToken { get; set; }
        string ServerVersion { get; set; }
        string ApplicationVersion { get; }
        TimeSpan UpdateInterval { get; set; }
    }
}