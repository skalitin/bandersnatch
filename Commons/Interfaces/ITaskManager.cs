using System;
using System.Collections.Generic;
using Quest.Cyclone.Client.Windows.Commons.Tasks;

namespace Quest.Cyclone.Client.Windows.Commons.Interfaces
{
    public interface ITaskManager
    {
        void RunTask(TaskBase task);
        void RunTask<T>() where T : TaskBase;

        void RunTask(Guid packageId, TaskBase task);
        void RunTask<T>(Guid packageId) where T : TaskBase;

        void RunPeriodicTask(TaskBase task, TimeSpan interval);
        void RunPeriodicTask<T>(TimeSpan interval) where T : TaskBase;

        void RunPeriodicTask(Guid packageId, TaskBase task, TimeSpan interval);
        void RunPeriodicTask<T>(Guid packageId, TimeSpan interval) where T : TaskBase;

        bool IsTaskRunning<T>() where T : TaskBase;
        bool IsTaskRunning<T>(Func<T, bool> predicate) where T : TaskBase;
        
        void CancelTask<T>();
    }
}