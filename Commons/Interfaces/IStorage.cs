﻿namespace Quest.Cyclone.Client.Windows.Commons.Interfaces
{
    public interface IStorage
    {
        void SaveSetting<T>(string name, T value);
        T GetSetting<T>(string name);
    }
}