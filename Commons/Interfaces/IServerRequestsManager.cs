using System;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;

namespace Quest.Cyclone.Client.Windows.Commons.Interfaces
{
    public interface IServerRequestsManager
    {
        RegisterResponse RegisterDevice();
        void UnsubscribeDevice(string username, string usertoken);
        ServerVersion GetServerVersion();
        ServerMetadata GetServerMetadata();
        PackagesResponse GetPackages();
        Package GetPackage(Guid packageId);
        byte[] GetImageData(String imageId);
        UpdatesResponse GetUpdates(long syncId);

        Alerts GetAlerts(Guid packageId, AlertStatus status, int count, long upto);
        Alert GetAlertDetails(Guid alertId);
        void AcknowledgeAlert(Guid alertId);
        void ResolveAlert(Guid alertId, string comment);
        void RunAlertAction(Guid actionId);

        DatasheetResponse GetDatasheets(Guid instanceId, Guid datasheetId, bool refresh);
        DatasheetResponse GetDatasheetContent(Guid instanceId, Guid datasheetId, Range range, bool refresh);
        DatasheetResponse GetDatasheetActions(Guid instanceId, Guid datasheetId, int index);
        DatasheetResponse RunDatasheetAction(Guid instanceId, Guid datasheetId, Guid actionId, int index);
        DatasheetResponse SetDatasheetParameters(Guid instanceId, Guid datasheetId, DatasheetParameterGroup parameters);
    }
}