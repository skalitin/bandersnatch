﻿using System.Threading.Tasks;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;

namespace Quest.Cyclone.Client.Windows.Commons
{
    public interface IIconDataRestorer
    {
        Icon RestoreIconData(Icon icon);
        Task<Icon> RestoreIconDataAsync(Icon icon);
    }
}