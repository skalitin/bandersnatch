﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quest.Cyclone.Client.Windows.Commons.Commands
{
    public interface IDatasheetCommandVisitor
    {
        void Visit(ShowDatasheetContentCommand command);
        void Visit(ShowDatasheetsCommand command);
    }
}
