using System;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;

namespace Quest.Cyclone.Client.Windows.Commons.Interfaces
{
    public interface IRefresh
    {
        void RefreshData(bool initial = false);
    }
}