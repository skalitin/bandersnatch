﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;

namespace Quest.Cyclone.Client.Windows.Commons.Interfaces
{
    public interface IAlertStorage
    {
        void RemoveAlerts();
        void RemoveAlerts(Guid packageId, IEnumerable<AlertId> alertIds);
        void RemoveAlerts(Guid packageId, AlertStatus status);

        void SetAlerts(Guid packageId, IEnumerable<Alert> alerts);

        void UpdateAlert(Guid packageId, Alert alert);
        void UpdateAlerts(Guid packageId, IEnumerable<Alert> alerts);

        BindableCollection<Alert> GetAlerts(Guid packageId);
        BindableCollection<Alert> GetAlerts(Guid packageId, AlertStatus status);
        ICollection<Alert> GetAlerts(AlertStatus status);
        bool HasAlerts(Guid packageId, AlertStatus status);
    }
}
