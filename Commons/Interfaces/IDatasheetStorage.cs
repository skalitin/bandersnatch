﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using Quest.Cyclone.Client.Windows.Commons.InterchangeFormats;

namespace Quest.Cyclone.Client.Windows.Commons.Interfaces
{
    public interface IDatasheetStorage
    {
        void LoadDatasheets(Guid datasheetId);
    }
}
