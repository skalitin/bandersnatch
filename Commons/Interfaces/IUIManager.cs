using System;
using Windows.UI.Core;

namespace Quest.Cyclone.Client.Windows.Commons.Interfaces
{
    public interface IUIManager
    {
        void RunOnUI(Action action);
        void ShowErrorByResourceName(string resourceName);
        void ShowErrorByResourceNameAndFormat(string resourceName, params object[] args);
        void ShowError(string message);
        void ShowError(Exception e);
        void ShowToast(string message);
        string GetStringFromResource(string resourceName);
    }
}